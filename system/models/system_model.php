<?php

class System_Model extends MVC_Model {
  public function checkUser($data){
    $this->db->query_one("SELECT id FROM users WHERE username = ?", array($data));
    return $this->db->num_rows();
  }

  public function checkPassword($data){
    $get = $this->db->query_one("SELECT password FROM users WHERE username = ?", array($data["username"]));

    //Converted the password verification to md5
    if(md5($data["password"]) == $get["password"]){
      return true;
    }else{
      return false;
    }
    //EDITED
    //$check = password_verify($data["password"], $get["password"]);
  }

  public function checkEmployeeNumber($data){
    $this->db->query("SELECT id FROM employees WHERE eid = ?", array($data));
    return $this->db->num_rows();
  }

  public function checkAccountUsername($data){
    $this->db->query("SELECT id FROM users WHERE username = ?", array($data));
    return $this->db->num_rows();
  }

  public function addBypass($data){
    return $this->db->insert('attendance_logs', array(
      "eid" => $data["eid"],
      "access_type" => $data["type"],
      "access_image" => "",
      "date" => date("Y-m-d"),
      "timestamp" => $data["timestamp"]
    ));
  }

  public function getUser($data){
    return $this->db->query_one("SELECT id, username FROM users WHERE username = ?", array($data));
  }

  public function getAccessToday($data){
    $this->db->query("SELECT id FROM attendance_logs WHERE eid = ? AND date = ?", array($data["eid"], $data["date"]));
    return $this->db->num_rows();
  }

  public function getSettings(){
    $query = "
      SELECT s.* 
      FROM settings s
    ";
    $this->db->query($query);
    if($this->db->num_rows() > 0) {
      while ($row = $this->db->next())
        $results[$row["name"]] = $row["value"];
      return $results;
    } else {
      return array();
    }
  }

  public function getAttendance($site_url = site_url){
    $query = "
      SELECT a.id AS id, a.eid AS eid, CONCAT(UCASE(LEFT(e.lname, 1)), SUBSTRING(e.lname, 2), ', ', UCASE(LEFT(e.fname, 1)), SUBSTRING(e.fname, 2), ' ', UCASE(LEFT(e.mname, 1)), SUBSTRING(e.mname, 2)) AS name, DATE_FORMAT(a.timestamp, '%M %d, %Y %r') AS date, DATE_FORMAT(a.timestamp, '%m-%d-%Y') AS fdate, 
            CASE a.access_image
              WHEN '' THEN CONCAT('<a href=\"', '{$site_url}/uploads/access/', 'no-image.jpg', '\" data-lightbox=\"image\"><img src=\"', '{$site_url}/uploads/access/', 'no-image.jpg', '\" style=\"width:30px\"></a>')
            ELSE CONCAT('<a href=\"', '{$site_url}/uploads/access/', a.access_image, '\" data-lightbox=\"image\"><img src=\"', '{$site_url}/uploads/access/', a.access_image, '\" style=\"width:30px\"></a>')
              END AS image,
            CASE a.access_type
              WHEN 1 THEN 'Time In'
              WHEN 2 THEN 'Time Out'
            ELSE 'Unknown'
              END AS access
      FROM attendance_logs a
      LEFT JOIN employees e ON a.eid = e.eid
      ORDER BY a.date DESC
    ";
    $this->db->query($query);
    if($this->db->num_rows() > 0) {
      while ($row = $this->db->next())
        $results[] = $row;
      return $results;
    } else {
      return array();
    }
  }

  public function getEmployees($site_url = site_url){
    $query = "
      SELECT e.id AS id, e.eid AS eid, CONCAT(UCASE(LEFT(e.lname, 1)), SUBSTRING(e.lname, 2), ', ', UCASE(LEFT(e.fname, 1)), SUBSTRING(e.fname, 2), ' ', UCASE(LEFT(e.mname, 1)), SUBSTRING(e.mname, 2)) AS name, CONCAT('₱', FORMAT(e.basic_pay, 0)) AS basic, p.name AS position, d.name AS department, e.type AS type_num, e.schedule AS schedule,
        CASE e.type
          WHEN 2 THEN 'Teaching'
          WHEN 3 THEN 'Division'
          ELSE 'Non-Teaching'
        END AS type,
        CONCAT(
          '<div class=\"btn-group\">', '<button class=\"btn btn-success btn-sm\" onclick=\"window.location = ', '\'{$site_url}/admin/employee/', e.id, '\'', '\"><i class=\"fa fa-edit\"></i> Edit</button>', '<button class=\"btn btn-danger btn-sm\" onclick=\"$.get(site_url + \'/ajax/delete_employee\',' ' {id:', e.id, '}, function(result){ var data = JSON.parse(result); if(data.result == \'success\'){ swal({title: \'Success!\',text: data.message,closeOnClickOutside: false,closeOnEsc: false,icon: \'success\',button: false}); setInterval(function(){ location.reload(true); }, 3000); } else { swal({title: \'Error!\',text: data.message,timer: 3000,icon: \'warning\',button: false}); }})\"><i class=\"fa fa-trash\"></i> Delete</button>', '</div>'
        ) AS options
      FROM employees e
      LEFT JOIN positions p ON e.position = p.id
      LEFT JOIN departments d ON e.department = d.id
      ORDER BY id DESC
    ";
    $this->db->query($query);
    if($this->db->num_rows() > 0) {
      while ($row = $this->db->next())
        $results[] = $row;
      return $results;
    } else {
      return array();
    }
  }

  public function getEmployeeInfo($data){
    return $this->db->query_one("SELECT * FROM employees WHERE id = ?", array($data));
  }

  public function getPositions($site_url = site_url){
    $query = "
      SELECT p.id AS id, p.name AS name, p.description AS description,
        CONCAT(
          '<div class=\"btn-group\">', '<button class=\"btn btn-success btn-sm\" onclick=\"window.location = ', '\'{$site_url}/admin/position/', p.id, '\'', '\"><i class=\"fa fa-edit\"></i> Edit</button>', '<button class=\"btn btn-danger btn-sm\" onclick=\"$.get(site_url + \'/ajax/delete_position\',' ' {id:', p.id, '}, function(result){ var data = JSON.parse(result); if(data.result == \'success\'){ swal({title: \'Success!\',text: data.message,closeOnClickOutside: false,closeOnEsc: false,icon: \'success\',button: false}); setInterval(function(){ location.reload(true); }, 3000); } else { swal({title: \'Error!\',text: data.message,timer: 3000,icon: \'warning\',button: false}); }})\"><i class=\"fa fa-trash\"></i> Delete</button>', '</div>'
        ) AS options
      FROM positions p
      ORDER BY p.id DESC
    ";
    $this->db->query($query);
    if($this->db->num_rows() > 0) {
      while ($row = $this->db->next())
        $results[] = $row;
      return $results;
    } else {
      return array();
    }
  }

  public function getDepartments($site_url = site_url){
    $query = "
      SELECT d.id AS id, d.name AS name, d.description AS description,
        CONCAT(
          '<div class=\"btn-group\">', '<button class=\"btn btn-success btn-sm\" onclick=\"window.location = ', '\'{$site_url}/admin/department/', d.id, '\'', '\"><i class=\"fa fa-edit\"></i> Edit</button>', '<button class=\"btn btn-danger btn-sm\" onclick=\"$.get(site_url + \'/ajax/delete_department\',' ' {id:', d.id, '}, function(result){ var data = JSON.parse(result); if(data.result == \'success\'){ swal({title: \'Success!\',text: data.message,closeOnClickOutside: false,closeOnEsc: false,icon: \'success\',button: false}); setInterval(function(){ location.reload(true); }, 3000); } else { swal({title: \'Error!\',text: data.message,timer: 3000,icon: \'warning\',button: false}); }})\"><i class=\"fa fa-trash\"></i> Delete</button>', '</div>'
        ) AS options
      FROM departments d
      ORDER BY d.id DESC
    ";
    $this->db->query($query);
    if($this->db->num_rows() > 0) {
      while ($row = $this->db->next())
        $results[] = $row;
      return $results;
    } else {
      return array();
    }
  }

  public function getPositionInfo($data){
    return $this->db->query_one("SELECT * FROM positions WHERE id = ?", array($data));
  }

  public function getPositionSelect(){
    $this->db->query("SELECT id, name FROM positions ORDER BY id ASC");
    if($this->db->num_rows() > 0) {
      while ($row = $this->db->next())
        $results[$row["id"]] = $row["name"];
      return $results;
    } else {
      return array();
    }
  }

  public function getDepartmentInfo($data){
    return $this->db->query_one("SELECT * FROM departments WHERE id = ?", array($data));
  }

  public function getDepartmentSelect(){
    $this->db->query("SELECT id, name FROM departments ORDER BY id ASC");
    if($this->db->num_rows() > 0) {
      while ($row = $this->db->next())
        $results[$row["id"]] = $row["name"];
      return $results;
    } else {
      return array();
    }
  }

  public function getHolidays($site_url = site_url){
    $query = "
      SELECT h.id AS id, DATE_FORMAT(CONCAT('0000-', h.date), '%b %e') AS date, h.name AS name,
        CONCAT(
          '<div class=\"btn-group\">', '<button class=\"btn btn-success btn-sm\" onclick=\"window.location = ', '\'{$site_url}/admin/holiday/', h.id, '\'', '\"><i class=\"fa fa-edit\"></i> Edit</button>', '<button class=\"btn btn-danger btn-sm\" onclick=\"$.get(site_url + \'/ajax/delete_holiday\',' ' {id:', h.id, '}, function(result){ var data = JSON.parse(result); if(data.result == \'success\'){ swal({title: \'Success!\',text: data.message,closeOnClickOutside: false,closeOnEsc: false,icon: \'success\',button: false}); setInterval(function(){ location.reload(true); }, 3000); } else { swal({title: \'Error!\',text: data.message,timer: 3000,icon: \'warning\',button: false}); }})\"><i class=\"fa fa-trash\"></i> Delete</button>', '</div>'
        ) AS options
      FROM holidays h
      ORDER BY h.id DESC
    ";
    $this->db->query($query);
    if($this->db->num_rows() > 0) {
      while ($row = $this->db->next())
        $results[] = $row;
      return $results;
    } else {
      return array();
    }
  }

  public function getHolidayInfo($data){
    return $this->db->query_one("SELECT * FROM holidays WHERE id = ?", array($data));
  }

  public function getAccounts($site_url = site_url){
    $query = "
      SELECT u.id AS id, u.username AS username,
        CONCAT(
          '<div class=\"btn-group\">', '<button class=\"btn btn-success btn-sm\" onclick=\"window.location = ', '\'{$site_url}/admin/account/', u.id, '\'', '\"><i class=\"fa fa-edit\"></i> Edit</button>', '<button class=\"btn btn-danger btn-sm\" onclick=\"$.get(site_url + \'/ajax/delete_account\',' ' {id:', u.id, '}, function(result){ var data = JSON.parse(result); if(data.result == \'success\'){ swal({title: \'Success!\',text: data.message,closeOnClickOutside: false,closeOnEsc: false,icon: \'success\',button: false}); setInterval(function(){ location.reload(true); }, 3000); } else { swal({title: \'Error!\',text: data.message,timer: 3000,icon: \'warning\',button: false}); }})\"><i class=\"fa fa-trash\"></i> Delete</button>', '</div>'
        ) AS options
      FROM users u
      ORDER BY u.id DESC
    ";
    $this->db->query($query);
    if($this->db->num_rows() > 0) {
      while ($row = $this->db->next())
        $results[] = $row;
      return $results;
    } else {
      return array();
    }
  }

  public function getAccountInfo($data){
    return $this->db->query_one("SELECT * FROM users WHERE id = ?", array($data));
  }

  public function getMonitors(){
    $query = "
      SELECT m.id AS id, m.name AS name,
        CONCAT(
          '<div class=\"btn-group\">', '<button class=\"btn btn-success btn-sm\"><i class=\"fa fa-edit\"></i> Edit</button>', '<button class=\"btn btn-danger btn-sm\"><i class=\"fa fa-trash\"></i> Delete</button>', '</div>'
        ) AS options
      FROM monitors m
      ORDER BY m.id DESC
    ";
    $this->db->query($query);
    if($this->db->num_rows() > 0) {
      while ($row = $this->db->next())
        $results[] = $row;
      return $results;
    } else {
      return array();
    }
  }

  public function getReportEmployees($data){
    if($data["type"] == 0 && $data["department"] == 0){
      $where = "";
    } else if($data["department"] == 0 && $data["type"] !== 0){
      $where = "WHERE type = {$data["type"]}";
    } else if($data["department"] !== 0 && $data["type"] == 0){
      $where = "WHERE department = {$data["department"]}";
    } else {
      $where = "WHERE department = {$data["department"]} AND type = {$data["type"]}";
    }
    $query = "
      SELECT e.*, d.name AS department
      FROM employees e
      LEFT JOIN departments d ON e.department = d.id
      {$where}
      ORDER BY e.lname ASC
    ";
    $this->db->query($query);
    if($this->db->num_rows() > 0) {
      while ($row = $this->db->next())
        $results[] = $row;
      return $results;
    } else {
      return array();
    }
  }

  public function getTimeRecord($data){
    $query = "
      SELECT DATE_FORMAT(a.timestamp, '%H:%i:%s') AS time, a.access_type AS type, a.timestamp AS timestamp, a.ismanual AS manual
      FROM attendance_logs a
      WHERE eid = ? AND date = ?
      ORDER BY a.timestamp ASC
    ";
    $this->db->query($query, array($data["eid"], $data["date"]));
    if($this->db->num_rows() > 0) {
      while ($row = $this->db->next())
        $results[] = $row;
      return $results;
    } else {
      return array();
    }
  }

  public function getTimeRecordCheck($data){
    $query = "
      SELECT DATE_FORMAT(a.timestamp, '%H:%i:%s') AS time, a.access_type AS type, a.timestamp AS timestamp
      FROM attendance_logs a
      WHERE eid = ? AND date = ?
      ORDER BY a.timestamp ASC
    ";
    $this->db->query($query, array($data["eid"], $data["date"]));
    if($this->db->num_rows() > 0) {
      while ($row = $this->db->next())
        $results[] = $row["type"];
      return $results;
    } else {
      return array();
    }
  }

  public function checkTimeRecord($data){
    $query = "
      SELECT DATE_FORMAT(a.timestamp, '%H:%i:%s') AS time, a.access_type AS type, a.timestamp AS timestamp
      FROM attendance_logs a
      WHERE eid = ? AND date = ?
      ORDER BY a.timestamp ASC
    ";
    $this->db->query($query, array($data["eid"], $data["date"]));
    if($this->db->num_rows() > 0) {
      while ($row = $this->db->next())
        $results[$row["timestamp"]] = $row;
      return $results;
    } else {
      return array();
    }
  }

  public function getHolidaysDTR(){
    $query = "
      SELECT id, date, name FROM holidays
    ";
    $this->db->query($query);
    if($this->db->num_rows() > 0) {
      while ($row = $this->db->next())
        $results[] = $row["date"];
      return $results;
    } else {
      return array();
    }
  }

  public function getEmployeeSchedule($data){
    return $this->db->query_one("SELECT schedule FROM employees WHERE eid = ?", array($data));
  }

  public function saveSettings($data){
    foreach($data AS $key => $value){
      $this->db->where("name", $key);
      if($this->db->update("settings", array("value" => $value)))
        $status = true;
      else
        $status = false;
    }
    return $status;
  }

  public function addEmployee($data){
    return $this->db->insert("employees", array(
      "eid" => strtoupper($data["eid"]),
      "rfid" => NULL,
      "type" => $data["type"],
      "basic_pay" => $data["basic"],
      "position" => $data["position"],
      "department" => $data["department"],
      "fname" => $data["fname"],
      "mname" => $data["mname"],
      "lname" => $data["lname"],
      "fid" => NULL,
      "ftemplate" => NULL,
      "schedule" => $data["schedule"]
    ));
  }

  public function addPosition($data){
    return $this->db->insert("positions", array(
      "name" => $data["name"],
      "description" => $data["description"]
    ));
  }

  public function addDepartment($data){
    return $this->db->insert("departments", array(
      "name" => $data["name"],
      "description" => $data["description"]
    ));
  }

  public function addHoliday($data){
    return $this->db->insert("holidays", array(
      "name" => $data["name"],
      "date" => $data["holiday"]
    ));
  }

  public function addAccount($data){
    return $this->db->insert("users", array(
      "username" => $data["username"], // EDITED
      "password" => md5($data["password"]) //password_hash($data["password"], PASSWORD_DEFAULT)

    ));
  }
  
  public function editEmployee($data){
    $this->db->where("id", $data["id"]);
    if(empty($data["eid"])){
      return $this->db->update("employees", array(
        "fname" => $data["fname"],
        "mname" => $data["mname"],
        "lname" => $data["lname"],
        "position" => $data["position"],
        "department" => $data["department"],
        "type" => $data["type"],
        "basic_pay" => $data["basic"],
        "schedule" => $data["schedule"]
      ));
    } else {
      return $this->db->update("employees", array(
        "eid" => $data["eid"],
        "fname" => $data["fname"],
        "mname" => $data["mname"],
        "lname" => $data["lname"],
        "position" => $data["position"],
        "department" => $data["department"],
        "type" => $data["type"],
        "basic_pay" => $data["basic"],
        "schedule" => $data["schedule"]
      ));
    }
  }

  public function editPosition($data){
    $this->db->where("id", $data["id"]);
    return $this->db->update("positions", array(
      "name" => $data["name"],
      "description" => $data["description"]
    ));
  }

  public function editDepartment($data){
    $this->db->where("id", $data["id"]);
    return $this->db->update("departments", array(
      "name" => $data["name"],
      "description" => $data["description"]
    ));
  }

  public function editHoliday($data){
    $this->db->where("id", $data["id"]);
    return $this->db->update("holidays", array(
      "name" => $data["name"],
      "date" => $data["holiday"]
    ));
  }

  public function editAccount($data){
    $this->db->where("id", $data["id"]);
    if(!empty($data["password"])){
      return $this->db->update("users", array(
        "username" => $data["username"],
        "password" => md5($data["password"]) //password_hash($data["password"], PASSWORD_DEFAULT)
      ));
    } else {
      return $this->db->update("users", array(
        "username" => $data["username"]
      ));
    }
  }

  public function deleteEmployee($data){
    $this->db->where("id", $data);
    return $this->db->delete("employees");
  }

  public function deletePosition($data){
    $this->db->where("id", $data);
    return $this->db->delete("positions");
  }

  public function deleteDepartment($data){
    $this->db->where("id", $data);
    return $this->db->delete("departments");
  }

  public function deleteHoliday($data){
    $this->db->where("id", $data);
    return $this->db->delete("holidays");
  }

  public function deleteAccount($data){
    $this->db->where("id", $data);
    return $this->db->delete("users");
  }
}