<?php

class Api_Model extends MVC_Model {
	public function checkEmployeeNumber($data){
		$this->db->query("SELECT id FROM employees WHERE eid = ?", array($data));
		return $this->db->num_rows();
	}

	public function checkFingerprintId($data){
		$this->db->query("SELECT id FROM employees WHERE fid = ?", array($data));
		return $this->db->num_rows();
	}

	public function checkEmployeeRfid($data){
		$this->db->query("SELECT id FROM employees WHERE rfid = ?", array($data));
		return $this->db->num_rows();
	}

	public function verifyRFID($data){
		$this->db->query("SELECT id FROM employees WHERE rfid = ?", array($data));
		return $this->db->num_rows();
	}

	public function verifyFingerprint($data){
		$this->db->query("SELECT id FROM employees WHERE fid = ?", array($data));
		return $this->db->num_rows();
	}

	public function verifyEmployeeNumber($data){
		$this->db->query("SELECT id FROM employees WHERE eid = ?", array($data));
		return $this->db->num_rows();
	}

	public function setRfidToNull($data){
		$this->db->where("rfid", $data);
		$this->db->update('employees', array(
			"rfid" => NULL
		));
    	return $this->db->num_rows();
	}

	public function setFingerprintToNull($data){
		$this->db->where("fid", $data);
		$this->db->update('employees', array(
			"fid" => NULL
		));
    	return $this->db->num_rows();
	}

	public function getEmployeeDataByRFID($data){
		return $this->db->query_one("SELECT e.id AS id, e.eid AS employee_no, e.rfid AS rfid, e.type AS type, e.basic_pay AS basic, e.department AS department, e.position AS position, e.fname AS fname, e.mname AS mname, e.lname AS lname, IF(e.fid IS NOT NULL, e.fid, '') AS finger_print_id, IF(e.ftemplate IS NOT NULL, e.ftemplate, '') AS finger_print_template FROM employees e WHERE rfid = ?", array($data));
	}

	public function getEmployeeDataByFingerprint($data){
		return $this->db->query_one("SELECT e.id AS id, e.eid AS employee_no, e.rfid AS rfid, e.type AS type, e.basic_pay AS basic, e.department AS department, e.position AS position, e.fname AS fname, e.mname AS mname, e.lname AS lname, IF(e.fid IS NOT NULL, e.fid, '') AS finger_print_id, IF(e.ftemplate IS NOT NULL, e.ftemplate, '') AS finger_print_template FROM employees e WHERE fid = ?", array($data));
	} 

	public function getEmployeeData($data){
		$query = "
			SELECT e.id AS id, e.eid AS employee_no, e.rfid AS rfid, e.basic_pay AS basic, d.name AS department, e.position AS position, e.fname AS fname, e.mname AS mname, e.lname AS lname, IF(e.fid IS NOT NULL, e.fid, '') AS finger_print_id, IF(e.ftemplate IS NOT NULL, e.ftemplate, '') AS finger_print_template,
				CASE e.type
	              WHEN 1 THEN 'Non-Teaching'
	              WHEN 2 THEN 'Teaching'
	              WHEN 3 THEN 'Division'
	            ELSE 'Unknown'
	              END AS type 
			FROM employees e
			INNER JOIN departments d ON e.department = d.id
			WHERE e.eid = ?
		";
		return $this->db->query_one($query, array($data));
	}

	public function getDepartments(){
		$query = "
		  SELECT d.id AS id, d.name AS name
		  FROM departments d
		  ORDER BY d.id DESC
		";
		$this->db->query($query);
		if($this->db->num_rows() > 0) {
		  while ($row = $this->db->next())
		    $results[] = $row;
		  return $results;
		} else {
		  return array();
		}
	}

	public function getPositions(){
		$query = "
		  SELECT p.id AS id, p.name AS name
		  FROM positions p
		  ORDER BY p.id DESC
		";
		$this->db->query($query);
		if($this->db->num_rows() > 0) {
		  while ($row = $this->db->next())
		    $results[] = $row;
		  return $results;
		} else {
		  return array();
		}
	}

	public function getFingerprints(){
		$query = "
		  SELECT e.fid AS id, e.ftemplate AS fp
		  FROM employees e
		  WHERE e.fid IS NOT NULL AND e.ftemplate IS NOT NULL
		";
		$this->db->query($query);
		if($this->db->num_rows() > 0) {
		  while ($row = $this->db->next())
		    $results[] = $row;
		  return $results;
		} else {
		  return array();
		}
	}

	public function addEmployee($data){
		return $this->db->insert("employees", array(
			"eid" => $data["employee_no"],
			"type" => $data["type"],
			"basic_pay" => $data["basic_pay"],
			"position" => $data["position"],
			"department" => $data["department"],
			"fname" => $data["fname"],
			"mname" => $data["mname"],
			"lname" => $data["lname"],
			"rfid" => $data["rfid"],
			"fid" => $data["finger_print_id"],
			"ftemplate" => (empty($data["fingerprint"]) ? NULL : $data["fingerprint"])
		));
	}

	public function update($data){
		$this->db->where("eid", $data["employee_no"]);
		if($data["type"] == 1){
			$this->db->update('employees', array(
    			"rfid" => $data["id"]
    		));
		} else {
			$this->db->update('employees', array(
    			"fid" => $data["id"],
    			"ftemplate" => $data["ftemplate"]
    		));
		}
    	return $this->db->num_rows();
	}

	public function removeExistingFingerprint($data){
		$this->db->where("fid", $data);
		$this->db->update('employees', array(
    			"fid" => NULL,
    			"ftemplate" => NULL
    		));
		return $this->db->num_rows();
	}

	public function updateFingerprint($data){
		$this->db->where("fid", $data["fid"]);
		return $this->db->update("employees", array(
			"fid" => $data["fid"],
			"ftemplate" => $data["ftemplate"]
		));
	}

	public function insertAccess($data){
	    return $this->db->insert('attendance_logs', array(
	      "eid" => $data["employee_no"],
	      "access_type" => $data["access_type"],
	      "access_image" => "",
	      "date" => date("Y-m-d"),
	      "timestamp" => $data["timestamp"]
	      ));
  	}	

  	public function updateAccess($data){
  		$this->db->where("eid", $data["employee_no"]);
  		$this->db->where("timestamp", $data["date"]);
     	$this->db->update('attendance_logs', array(
    		"access_image" => $data["image"]
    	));
    	return $this->db->num_rows();
  	}
}