<?php

class Panel_Model extends MVC_Model {
	public function verifyEmployeeNumber($data){
		$this->db->query("SELECT id FROM employees WHERE eid = ?", array($data));
		return $this->db->num_rows();
	}
	public function getAttendance($data){
		$query = "
	      SELECT a.eid AS employee_number, CONCAT(UCASE(LEFT(e.lname, 1)), SUBSTRING(e.lname, 2), ', ', UCASE(LEFT(e.fname, 1)), SUBSTRING(e.fname, 2), ' ', UCASE(LEFT(e.mname, 1)), SUBSTRING(e.mname, 2)) AS name, 
				CASE a.access_type
	              WHEN 1 THEN 'Time In'
	              WHEN 2 THEN 'Time Out'
	            ELSE 'Unknown'
	              END AS access_type,
	      		DATE_FORMAT(a.timestamp, '%M %d, %Y %r') AS date
	      FROM attendance_logs a
	      LEFT JOIN employees e ON a.eid = e.eid
	      WHERE e.eid = ? AND MONTH(a.date) = ? 
	      ORDER BY a.timestamp DESC
	    ";
	    $this->db->query($query, array($data["eid"], $data["month"]));
	    if($this->db->num_rows() > 0) {
	      while ($row = $this->db->next())
	        $results[] = $row;
	      return $results;
	    } else {
	      return array();
	    }
	}
}