<?php
/**
 * Database configuration
 * @package NTEK MVC
 * @author Norielle Cruz <nogats07@gmail.com>
 */

$config['default']['plugin'] = 'MVC_PDO'; // plugin for db access
$config['default']['type'] = 'mysql';      // connection type
$config['default']['host'] = 'localhost';  // db hostname
$config['default']['name'] = 'marikina';     // db name
$config['default']['user'] = 'root';     // db username
$config['default']['pass'] = '';     // db password
$config['default']['persistent'] = false;  // db connection persistence?
