<?php
/**
 * Autoload configuration
 * @package NTEK MVC
 * @author Norielle Cruz <nogats07@gmail.com>
 */

/* auto-loaded libraries */
$config['libraries'] = array(
    array("Smarty_Wrapper", "smarty"),
    array("SimplePHPCache", "cache"),
    array("Upload", "upload"),
    array("uri", "url")
    );
    
/* auto-loaded models */
$config['models'] = array(
  array("System_Model", "system"),
  array("Api_Model", "api"),
  array("Panel_Model", "panel")
);

/* auto-loaded scripts */
$config['scripts'] = array("helper", "tools", "password");