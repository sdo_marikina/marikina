<?php
/**
 * admin.php
 * Admin controller
 * @package NTEK MVC
 * @author Norielle Cruz <nogats07@gmail.com>
 */

class Admin_Controller extends MVC_Controller {
  public function index(){
    if(isset($_SESSION["logged"]))
      die(go(site_url."/admin/dashboard"));

    $data = array(
        "page" => "login"
    );

    $this->smarty->assign($data);
    $this->smarty->display("default/header.html");
    $this->smarty->display("default/pages/login.html");
    $this->smarty->display("default/footer.html");
  }

  public function logout(){
    if(!isset($_SESSION["logged"]))
      die(go(site_url));

    session_destroy();
    die(go(site_url));
  }

  public function dashboard(){
    if(!isset($_SESSION["logged"]))
      die(go(site_url));

    $data = array(
        "page" => "home"
    );

    $settings = $this->system->getSettings();

    $departments = $this->system->getDepartmentSelect();
    $departments[0] = "All";

    $types = array(
      0 => "All",
      1 => "Non-Teaching",
      2 => "Teaching",
      3 => "Division"
    );

    $this->smarty->assign($data);
    $this->smarty->assign($_SESSION["logged"]);
    $this->smarty->assign("settings", $settings);
    $this->smarty->assign("departments", $departments);
    $this->smarty->assign("types", $types);
    $this->smarty->display("default/header.html");
    $this->smarty->display("default/pages/home.html");
    $this->smarty->display("default/footer.html");
  }

  public function history(){
    if(!isset($_SESSION["logged"]))
      die(go(site_url));

    $data = array(
        "page" => "history"
    );

    $settings = $this->system->getSettings();

    $departments = $this->system->getDepartmentSelect();
    $departments[0] = "All";

    $types = array(
      0 => "All",
      1 => "Non-Teaching",
      2 => "Teaching",
      3 => "Division"
    );

    $this->smarty->assign($data);
    $this->smarty->assign($_SESSION["logged"]);
    $this->smarty->assign("settings", $settings);
    $this->smarty->assign("departments", $departments);
    $this->smarty->assign("types", $types);
    $this->smarty->display("default/header.html");
    $this->smarty->display("default/pages/history.html");
    $this->smarty->display("default/footer.html");
  }

  public function employees(){
    if(!isset($_SESSION["logged"]))
      die(go(site_url));

    $data = array(
      "page" => "employees"
    );

    $settings = $this->system->getSettings();
    $positions = $this->system->getPositionSelect();
    $departments = $this->system->getDepartmentSelect();

    $types = array(
      1 => "Non-Teaching",
      2 => "Teaching",
      3 => "Division"
    );

    $this->smarty->assign($data);
    $this->smarty->assign($_SESSION["logged"]);
    $this->smarty->assign("settings", $settings);
    $this->smarty->assign("positions", $positions);
    $this->smarty->assign("departments", $departments);
    $this->smarty->assign("types", $types);
    $this->smarty->display("default/header.html");
    $this->smarty->display("default/pages/employees.html");
    $this->smarty->display("default/footer.html");
  }

  public function employee(){
    if(!isset($_SESSION["logged"]))
      die(go(site_url));

    if(!isInt($this->url->segment(4))){
      die(go(site_url."/admin/employees"));
    }

    $data = array(
        "page" => "employee"
    );

    $employee = $this->system->getEmployeeInfo($this->url->segment(4));
    $settings = $this->system->getSettings();
    $positions = $this->system->getPositionSelect();
    $departments = $this->system->getDepartmentSelect();
    if($employee["schedule"] == NULL){
      $schedule = array("8:30 AM", "5:30 PM", "12:00 PM", "1:00 PM");
    } else {
      $schedule = explode(",", $employee["schedule"]);
    }

    $types = array(
      1 => "Non-Teaching",
      2 => "Teaching",
      3 => "Division"
    );

    $this->smarty->assign($data);
    $this->smarty->assign($_SESSION["logged"]);
    $this->smarty->assign("employee", $employee);
    $this->smarty->assign("settings", $settings);
    $this->smarty->assign("positions", $positions);
    $this->smarty->assign("departments", $departments);
    $this->smarty->assign("schedule", $schedule);
    $this->smarty->assign("types", $types);
    $this->smarty->display("default/header.html");
    $this->smarty->display("default/pages/employee.html");
    $this->smarty->display("default/footer.html");
  }

  public function positions(){
    if(!isset($_SESSION["logged"]))
      die(go(site_url));

    $data = array(
      "page" => "positions"
    );

    $settings = $this->system->getSettings();

    $this->smarty->assign($data);
    $this->smarty->assign($_SESSION["logged"]);
    $this->smarty->assign("settings", $settings);
    $this->smarty->display("default/header.html");
    $this->smarty->display("default/pages/positions.html");
    $this->smarty->display("default/footer.html");
  }

  public function position(){
    if(!isset($_SESSION["logged"]))
      die(go(site_url));

    if(!isInt($this->url->segment(4))){
      die(go(site_url."/admin/positions"));
    }

    $data = array(
        "page" => "position"
    );

    $position = $this->system->getPositionInfo($this->url->segment(4));
    $settings = $this->system->getSettings();

    $this->smarty->assign($data);
    $this->smarty->assign($_SESSION["logged"]);
    $this->smarty->assign("position", $position);
    $this->smarty->assign("settings", $settings);
    $this->smarty->display("default/header.html");
    $this->smarty->display("default/pages/position.html");
    $this->smarty->display("default/footer.html");
  }

  public function departments(){
    if(!isset($_SESSION["logged"]))
      die(go(site_url));

    $data = array(
      "page" => "departments"
    );

    $settings = $this->system->getSettings();

    $this->smarty->assign($data);
    $this->smarty->assign($_SESSION["logged"]);
    $this->smarty->assign("settings", $settings);
    $this->smarty->display("default/header.html");
    $this->smarty->display("default/pages/departments.html");
    $this->smarty->display("default/footer.html");
  }

  public function department(){
    if(!isset($_SESSION["logged"]))
      die(go(site_url));

    if(!isInt($this->url->segment(4))){
      die(go(site_url."/admin/departments"));
    }

    $data = array(
        "page" => "department"
    );

    $department = $this->system->getDepartmentInfo($this->url->segment(4));
    $settings = $this->system->getSettings();

    $this->smarty->assign($data);
    $this->smarty->assign($_SESSION["logged"]);
    $this->smarty->assign("department", $department);
    $this->smarty->assign("settings", $settings);
    $this->smarty->display("default/header.html");
    $this->smarty->display("default/pages/department.html");
    $this->smarty->display("default/footer.html");
  }

  public function holidays(){
    if(!isset($_SESSION["logged"]))
      die(go(site_url));

    $data = array(
      "page" => "holidays"
    );

    $settings = $this->system->getSettings();

    $this->smarty->assign($data);
    $this->smarty->assign($_SESSION["logged"]);
    $this->smarty->assign("settings", $settings);
    $this->smarty->display("default/header.html");
    $this->smarty->display("default/pages/holidays.html");
    $this->smarty->display("default/footer.html");
  }

  public function holiday(){
    if(!isset($_SESSION["logged"]))
      die(go(site_url));

    if(!isInt($this->url->segment(4))){
      die(go(site_url."/admin/holidays"));
    }

    $data = array(
        "page" => "holiday"
    );

    $holiday = $this->system->getHolidayInfo($this->url->segment(4));
    $settings = $this->system->getSettings();

    $this->smarty->assign($data);
    $this->smarty->assign($_SESSION["logged"]);
    $this->smarty->assign("holiday", $holiday);
    $this->smarty->assign("settings", $settings);
    $this->smarty->display("default/header.html");
    $this->smarty->display("default/pages/holiday.html");
    $this->smarty->display("default/footer.html");
  }

  public function accounts(){
    if(!isset($_SESSION["logged"]))
      die(go(site_url));

    $data = array(
      "page" => "accounts"
    );

    $settings = $this->system->getSettings();

    $this->smarty->assign($data);
    $this->smarty->assign($_SESSION["logged"]);
    $this->smarty->assign("settings", $settings);
    $this->smarty->display("default/header.html");
    $this->smarty->display("default/pages/accounts.html");
    $this->smarty->display("default/footer.html");
  }

  public function account(){
    if(!isset($_SESSION["logged"]))
      die(go(site_url));

    if(!isInt($this->url->segment(4))){
      die(go(site_url."/admin/accounts"));
    }

    $data = array(
        "page" => "accounts"
    );

    $account = $this->system->getAccountInfo($this->url->segment(4));
    $settings = $this->system->getSettings();

    $this->smarty->assign($data);
    $this->smarty->assign($_SESSION["logged"]);
    $this->smarty->assign("account", $account);
    $this->smarty->assign("settings", $settings);
    $this->smarty->display("default/header.html");
    $this->smarty->display("default/pages/account.html");
    $this->smarty->display("default/footer.html");
  }

  public function form7(){
    if(!isset($_SESSION["logged"]))
      die(go(site_url));

    $data = array(
      "page" => "form7"
    );

    $this->smarty->assign($data);
    $this->smarty->assign($_SESSION["logged"]);
    $this->smarty->display("default/header.html");
    $this->smarty->display("default/pages/form7.html");
    $this->smarty->display("default/footer.html");
  }
}