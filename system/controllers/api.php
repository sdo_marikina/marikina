<?php
/**
 * apiphp
 * API controller
 * @package NTEK MVC
 * @author Norielle Cruz <nogats07@gmail.com>
 */

class Api_Controller extends MVC_Controller {
  public function index(){
   	header('Access-Control-Allow-Origin: *');
    die(json_encode(array("result" => "fail", "message" => "You are trying to mess up the system!")));
  }

  /**
    *@api {GET}/api/access Employee Access
    *@apiDescription Record the employee access time-in or time-out
    *@apiName Employee Access
    *@apiGroup Smartentry
    *@apiVersion 1.0.0
    *
    *@apiParam {int} employee_no Employee unique number. 
    *@apiParam {int} access_type Access type.
    *<br/> 1 = Time-in
    *<br/> 2 = Time-out
    *
    *@apiSuccess (SuccessResponse) {String} result Result handler of the API.
    *@apiSuccess (SuccessResponse) {String} timestamp The timestamp of insertion to database.
    *@apiSuccess (SuccessResponse) {String} message The result message.
    *
    *@apiSuccessExample Success Response:
    *{
    *  "result": "success",
    *  "timestamp": "2018-07-22 19:23:32",
    *  "data": "Access has been recorded!"
    *}
    *
    *@apiError (FailResponse) {String} result Result handler of the API.
    *
    *@apiErrorExample Fail Response:
    *{
    *  "result": "fail"
    *}
    *
    */
  public function access(){
  	header('Access-Control-Allow-Origin: *');
  	$data = $_GET;

  	if(!isset($data["employee_no"], $data["access_type"]))
  		die(json_encode(array("result" => "fail", "message" => "Some fields are not set!")));

  	if($this->api->checkEmployeeNumber($data["employee_no"]) > 0){
      switch($data["access_type"]){
        case 1:
          $access_type = "Time In";
          break;
        case 2:
          $access_type = "Time Out";
          break;
        default:
          $access_type = 1;
          break;
      }
      $employee = $this->api->getEmployeeData($data["employee_no"]);
      $attendance = $this->system->getAccessToday(array("eid" => $data["employee_no"], "date" => date("Y-m-d")));
      /*
      if($employee["type"] == 2){
        if($attendance >= 2){
          die(json_encode(array("result" => "fail")));
        }
      } else {
        if($attendance >= 4){
          die(json_encode(array("result" => "fail")));
        }
      }
      */
      $data["timestamp"] = date('Y-m-d G:i:s');
      prepend("{$data["employee_no"]}<=>{$employee["lname"]}, {$employee["fname"]} {$employee["mname"]}<=>{$access_type}<=>". date("F j, Y g:i:s A", strtotime($data["timestamp"])) ."\n", "system/data/attendance.log");
      $this->api->insertAccess($data);
  		die(json_encode(array("result" => "success", "timestamp" => $data["timestamp"], "message" => "Access has been recorded!")));
  	} else {
  		die(json_encode(array("result" => "fail")));
  	}
  }

  /**
    *@api {GET}/api/employee Employee Verification
    *@apiDescription Verify the employee RFID or Fingerprint
    *@apiName Employee Verification
    *@apiGroup Smartentry
    *@apiVersion 1.0.0
    *
    *@apiParam {int} type Verification type.
    *<br/> 1 = RFID
    *<br/> 2 = Fingerprint
    *<br/> 3 = Employee Number
    *@apiParam {int} id Verification id of RFID, Fingerprint or Employee Number.
    *
    *@apiSuccess (SuccessResponse) {String} result Result handler of the API.
	*@apiSuccess (SuccessResponse) {String} data Employee details.
    *
    *@apiSuccessExample Success Response:
    *{
    *  "result": "success",
    *  "data": {
    *    "id": "9",
    *    "employee_no": "1241241244",
    *    "rfid": "1234567",
    *    "type": "1",
    *    "basic_pay": "10000",
    *    "department": "1",
    *    "fname": "Test",
    *    "mname": "Moe",
    *    "lname": "Doe",
    *    "fid": "23",
    *    "ftemplate": "2038403_template.dat"
    *  }
    *}
    *
    *@apiError (FailResponse) {String} result Result handler of the API.
    *@apiError (FailResponse) {String} data Employee details.
    *
    *@apiErrorExample Fail Response:
    *{
    *  "result": "fail",
       "data": ""
    *}
    *
    */
  public function employee(){
  	header('Access-Control-Allow-Origin: *');
  	$data = $_GET;

  	if(!isset($data["type"], $data["id"]))
  		die(json_encode(array("result" => "fail", "message" => "Some fields are not set!")));

  	if(empty($data["id"]))
  		die(json_encode(array("result" => "fail", "message" => "This system is hackproof! Go away idiot!")));

  	if($data["type"] == 1){
  		if($this->api->verifyRFID($data["id"]) > 0){
  			die(json_encode(array("result" => "success", "data" => $this->api->getEmployeeDataByRFID($data["id"]))));
  		} else {
  			die(json_encode(array("result" => "fail", "data" => "")));
  		}
  	} else if($data["type"] == 2){
  		if($this->api->verifyFingerprint($data["id"]) > 0){
  			die(json_encode(array("result" => "success", "data" => $this->api->getEmployeeDataByFingerprint($data["id"]))));
  		} else {
  			die(json_encode(array("result" => "fail", "data" => "")));
  		}
  	} else {
      if($this->api->verifyEmployeeNumber($data["id"]) > 0){
        die(json_encode(array("result" => "success", "data" => $this->api->getEmployeeData($data["id"]))));
      } else {
        die(json_encode(array("result" => "fail", "data" => "")));
      }
    }
  }

  /**
	*@api {POST}/api/register Register
	*@apiDescription Register a new employee to the database
	*@apiName Register
	*@apiGroup Smartentry
	*@apiVersion 1.0.0
	*
	*@apiParam {int} employee_no Employee unique number.
	*@apiParam {int} type Employee type.
	*@apiParam {int} basic_pay Employee salary.
	*@apiParam {int} department Department of the employee.
	*@apiParam {String} fname Employee first name.
	*@apiParam {String} mname Employee middle name.
	*@apiParam {String} lname Employee last name.
	*@apiParam {int} rfid Employee card number.
	*@apiParam {String} finger_print_id Fingerprint unqiue id.
	*@apiParam {FILE} finger_print_template Fingerprint template file.
	*
	*@apiSuccess (SuccessResponse) {String} result Result handler of the API.
	*@apiSuccess (SuccessResponse) {String} message The result message
	*
	*@apiSuccessExample Success Response:
	*{
	*  "result": "success",
	*  "message": "Employee has been successfully added!"
	*}
	*
	*@apiSuccess (FailResponse) {String} result Result handler of the API.
	*@apiSuccess (FailResponse) {String} message The result message
	*
	*@apiErrorExample Fail Response:
	*{
	*  "result": "fail",
	*  "message": "RFID is already used by existing employee!"
	*}
	*
	*/
  public function register(){
  	header('Access-Control-Allow-Origin: *');
  	$data = $_POST;

  	if(!isset($data["employee_no"], $data["type"], $data["position"], $data["basic_pay"], $data["department"], $data["fname"], $data["mname"], $data["lname"])){
  		die(json_encode(array("result" => "fail", "message" => "Some required fields are not set!")));
  	} 

  	if(empty($data["employee_no"]))
  		die(json_encode(array("result" => "fail", "message" => "Employee number cannot be empty!")));

  	if(empty($data["type"]))
  		die(json_encode(array("result" => "fail", "message" => "Employee type cannot be empty!")));

  	if(empty($data["basic_pay"]))
  		die(json_encode(array("result" => "fail", "message" => "Employee salary cannot be empty!")));

    if(isset($data["rfid"])){
      if(empty($data["rfid"])){
        $data["rfid"] = NULL;
      } else {
        if($this->api->checkEmployeeRfid($data["rfid"]))
          die(json_encode(array("result" => "fail", "message" => "RFID is already used by existing employee!")));
      }
    } else {
      $data["rfid"] = NULL;
    }

    if(isset($data["finger_print_id"])){
      if(empty($data["finger_print_id"])){
        $data["finger_print_id"] = NULL;
      } else {
        if($this->api->checkFingerprintId($data["finger_print_id"]) > 0)
          $this->api->removeExistingFingerprint($data["finger_print_id"]);
      }
      if(isset($_FILES["finger_print_template"])){
      	if(!empty($_FILES["finger_print_template"]["tmp_name"]) || is_uploaded_file($_FILES["finger_print_template"]["tmp_name"]))
      		$data["fingerprint"] = moveFingerprint($_FILES["finger_print_template"]);
      }
    } else {
      $data["finger_print_id"] = NULL;
    }

  	if($this->api->checkEmployeeNumber($data["employee_no"]) > 0){
  		die(json_encode(array("result" => "fail", "message" => "Employee number is already used by existing employee!")));
  	} else {
  		if($this->api->addEmployee($data)){
  			die(json_encode(array("result" => "success", "message" => "Employee has been successfully added!")));
  		} else {
  			die(json_encode(array("result" => "fail", "message" => "Something went wrong!")));
  		}
  	}
  }

  /**
   *@api {POST}/api/update Update
   *@apiDescription Update employee rfid or fingerprint
   *@apiName Update
   *@apiGroup Smartentry
   *@apiVersion 1.0.0
   *
   *@apiParam {int} employee_no Employee unique number.
   *@apiParam {int} type Update type.
   *@apiParam {int} id rfid or fingerprint id.
   *
   *@apiSuccess (SuccessResponse) {String} result Result handler of the API.
   *@apiSuccess (SuccessResponse) {String} message The result message
   *
   *@apiSuccessExample Success Response:
   *{
   *  "result": "success",
   *  "message": "Employee RFID has been updated!"
   *}
   *
   *@apiSuccess (FailResponse) {String} result Result handler of the API.
   *@apiSuccess (FailResponse) {String} message The result message
   *
   *@apiErrorExample Fail Response:
   *{
   *  "result": "fail",
   *  "message": "Something went wrong!"
   *}
   *
   */
  public function update(){
    header('Access-Control-Allow-Origin: *');
    $data = $_POST;

    if(!isset($data["employee_no"], $data["type"], $data["id"])){
      die(json_encode(array("result" => "fail", "message" => "Some fields are not set!")));
    }

    if($this->api->checkEmployeeNumber($data["employee_no"]) > 0){
      if($data["type"] == 1){
        if($this->api->checkEmployeeRfid($data["id"]) > 0){
          $this->api->setRfidToNull($data["id"]);
        }
        if($this->api->update(array("employee_no" => $data["employee_no"], "type" => 1, "id" => $data["id"], "ftemplate" => NULL)) > 0)
          die(json_encode(array("result" => "success", "message" => "Employee RFID has been updated!")));
        else
          die(json_encode(array("result" => "fail", "message" => "Something went wrong!")));
      } else {
        if($this->api->checkFingerprintId($data["id"]) > 0){
          $this->api->setFingerprintToNull($data["id"]);
        }
        if(isset($_FILES["finger_print_template"])){
          if(!empty($_FILES["finger_print_template"]["tmp_name"]) || is_uploaded_file($_FILES["finger_print_template"]["tmp_name"])){
            $data["fingerprint"] = moveFingerprint($_FILES["finger_print_template"]);
          } else {
            $data["fingerprint"] = NULL;
          }
        }
        if($this->api->update(array("employee_no" => $data["employee_no"], "type" => 2, "id" => $data["id"], "ftemplate" => $data["fingerprint"])) > 0)
          die(json_encode(array("result" => "success", "message" => "Employee fingerprint has been updated!")));
        else
          die(json_encode(array("result" => "fail", "message" => "Something went wrong!")));
      }
    } else {
      die(json_encode(array("result" => "fail", "message" => "Employee not found!")));
    }
  }

  /**
   *@api {POST}/api/upload Upload
   *@apiDescription Upload a new fingerprint data
   *@apiName Upload
   *@apiGroup Smartentry
   *@apiVersion 1.0.0
   *
   *@apiParam {String} finger_print_id Fingerprint unqiue id.
   *@apiParam {FILE} finger_print_template Fingerprint template file.
   *
   *@apiSuccess (SuccessResponse) {String} result Result handler of the API.
   *@apiSuccess (SuccessResponse) {String} message The result message
   *
   *@apiSuccessExample Success Response:
   *{
   *  "result": "success",
   *  "message": "Fingerprint has been uploaded!"
   *}
   *
   *@apiSuccess (FailResponse) {String} result Result handler of the API.
   *@apiSuccess (FailResponse) {String} message The result message
   *
   *@apiErrorExample Fail Response:
   *{
   *  "result": "fail",
   *  "message": "Something went wrong!"
   *}
   *
   */
  public function upload(){
    header('Access-Control-Allow-Origin: *');
    $data = $_POST;

    if(!isset($data["finger_print_id"], $_FILES["finger_print_template"])){
      die(json_encode(array("result" => "fail", "message" => "Some fields are not set!")));
    }

    if($this->api->checkFingerprintId($data["finger_print_id"]) > 0){
      $employee = $this->api->getEmployeeDataByFingerprint($data["finger_print_id"]);
    } else {
      die(json_encode(array("result" => "fail", "message" => "No employee with this fingerprint!")));
    }

    if(!empty($_FILES["finger_print_template"]["tmp_name"]) || is_uploaded_file($_FILES["finger_print_template"]["tmp_name"])){
      $ftemplate = moveRenameFingerprint(array("ftemplate" => $_FILES["finger_print_template"], "eid" => $employee["employee_no"]));
    } else {
      die(json_encode(array("result" => "fail", "message" => "Fingerprint template is required!")));
    }

    if(!empty($ftemplate)){
      if($this->api->updateFingerprint(array("fid" => $data["finger_print_id"], "ftemplate" => $ftemplate))){
        die(json_encode(array("result" => "success", "message" => "Fingerprint has been uploaded!")));
      } else {
        die(json_encode(array("result" => "fail", "message" => "Something went wrong!")));
      }
    } else {
      die(json_encode(array("result" => "fail", "message" => "Something went wrong!")));
    }
  }

  /**
	*@api {GET}/api/departments Departments
	*@apiDescription Get the list of departments
	*@apiName Departments
	*@apiGroup Smartentry
	*@apiVersion 1.0.0
	*
	*@apiSuccessExample Success Response:
	*{
	* "result": "success",
	* "data": [
	*      {
	*          "id": "2",
	*          "name": "Office"
	*      },
	*      {
	*          "id": "1",
	*          "name": "Registrar"
	*      }
	*  ]
	*}
	*
	*/
  public function departments(){
  	header('Access-Control-Allow-Origin: *');
  	die(json_encode(array("result" => "success", "data" => $this->api->getDepartments())));
  }

  /**
	*@api {GET}/api/positions Positions
	*@apiDescription Get the list of positions
	*@apiName Positions
	*@apiGroup Smartentry
	*@apiVersion 1.0.0
	*
	*@apiSuccessExample Success Response:
	*{
	* "result": "success",
	* "data": [
	*      {
	*          "id": "2",
	*          "name": "Security"
	*      },
	*      {
	*          "id": "1",
	*          "name": "CEO"
	*      }
	*  ]
	*}
	*
	*/
  public function positions(){
  	header('Access-Control-Allow-Origin: *');
  	die(json_encode(array("result" => "success", "data" => $this->api->getPositions())));
  }

  /**
	*@api {GET}/api/types Employment Types
	*@apiDescription Get the list of employment types
	*@apiName Employment Types
	*@apiGroup Smartentry
	*@apiVersion 1.0.0
	*
	*@apiSuccessExample Success Response:
	*{
	*	 "result": "success",
	*  "data": [
	*      {
	*          "id": "1",
	*          "name": "Non-Teaching"
	*      },
	*      {
	*          "id": "2",
	*         "name": "Teaching"
	*      },
	*      {
	*          "id": "3",
	*          "name": "Division"
	*      }
	*   ]
	*}
	*
	*/
  public function types(){
  	header('Access-Control-Allow-Origin: *');
  	$type = array(
  		array(
  			"id" => "1",
  			"name" => "Non-Teaching"
  		),
  		array(
  			"id" => "2",
  			"name" => "Teaching"
  		),
  		array(
  			"id" => "3",
  			"name" => "Division"
  		)
  	);
  	die(json_encode(array("result" => "success", "data" => $type)));
  }

  /**
  *@api {POST}/api/uploadImage Upload Image
  *@apiDescription Upload captured image of employee when rfid access was used
  *@apiName Upload Image
  *@apiGroup Smartentry
  *@apiVersion 1.0.0
  *
  *@apiParam {int} employee_no Employee unique number.
  *@apiParam {String} date Access Date.
  *@apiParam {FILE} picture Captured image file.
  *
  *@apiSuccess (SuccessResponse) {String} result Result handler of the API.
  *@apiSuccess (SuccessResponse) {String} message The result message
  *
  *@apiSuccessExample Success Response:
  *{
  *  "result": "success",
  *  "message": "Captured image has been uploaded!"
  *}
  *
  *@apiSuccess (FailResponse) {String} result Result handler of the API.
  *@apiSuccess (FailResponse) {String} message The result message
  *
  *@apiErrorExample Fail Response:
  *{
  *  "result": "fail",
  *  "message": "No access log have matched the employee number and timestamp!"
  *}
  *
  */
  public function uploadImage(){
    header('Access-Control-Allow-Origin: *');
    $data = $_POST;

    if(!isset($data["employee_no"], $data["date"], $_FILES["picture"])){
      die(json_encode(array("result" => "fail", "message" => "Some fields are not set!")));
    }

    $picture = movePicture($_FILES["picture"]); 

    if($this->api->updateAccess(array("employee_no" => $data["employee_no"], "date" => $data["date"], "image" => $picture)) > 0){
      die(json_encode(array("result" => "success", "message" => "Captured image has been uploaded!")));
    } else {
      die(json_encode(array("result" => "fail", "message" => "No access log have matched the employee number and timestamp!")));
    }
  }

  /**
  *@api {POST}/api/getFingerPrintData Get Fingerprints
  *@apiDescription Get the list of fingerprints
  *@apiName Get Fingerprints
  *@apiGroup Smartentry
  *@apiVersion 1.0.0
  *
  *@apiSuccess (SuccessResponse) {String} result Handle the server response.
  *@apiSuccess (SuccessResponse) {Integer} total_count Handle the number of records.
  *@apiSuccess (SuccessResponse) {Object[]} list Handle the array of the finger print data.
  *@apiSuccess (SuccessResponse) {Integer} list.finger_print_id Data handler of the finger print id from the device registration.
  *@apiSuccess (SuccessResponse) {String} list.finger_print_template Handle the url for the download file.
  *@apiSuccess (SuccessResponse) {String} list.server_created_date Data handler of the server registration date.
  *
  *@apiSuccessExample Success Response:
  *{
  *"result": "success",
  *"total_count":14,
  *"list": [
  *    {
  *      "finger_print_id":"1",
  *      "finger_print_template":"192.168.1.166/marikina/finger_print_data/1111111_template_1.dat",
  *      "server_created_date":"2017-12-20 13:41:45"
  *    },
  *    {
  *      "finger_print_id":"8",
  *      "finger_print_template":"192.168.1.166/marikina/finger_print_data/1111111_template_1.dat",
  *      "server_created_date":"2017-12-20 10:49:23"
  *    }
  *  ]
  *}
  *
  *
  */
  public function getFingerPrintData(){
    header('Access-Control-Allow-Origin: *');
    $fingerprints = $this->api->getFingerprints();
    if(count($fingerprints) > 0){
      $fingerprint = array();
      foreach($fingerprints as $fp){
        if(!empty($fp["fp"])){
          if(file_exists("uploads/fingerprints/" . $fp["fp"])){
            $fingerprint[] = array(
              "finger_print_id" => $fp["id"],
              "finger_print_template" => site_url . "/uploads/fingerprints/" . $fp["fp"],
              "server_created_date" => date("Y-m-d H:i:s", filemtime("uploads/fingerprints/" . $fp["fp"]))
            );
          }
        }
      }
      die(json_encode(array("result" => "success", "total_count" => count($fingerprint), "list" => $fingerprint)));
    } else {
      die(json_encode(array("result" => "fail", "total_count" => 0, "list" => array())));
    }
  }

  /**
  *@api {POST}/api/getServerTime Get Server Date and Time
  *@apiDescription This API is use to get current server time and date from the server.
  *@apiName Get Get Server Time
  *@apiGroup Smartentry
  *@apiVersion 1.0.0
  *
  *@apiSuccess (SuccessResponse) {String} result Handle the server response.
  *@apiSuccess (SuccessResponse) {String} server_time Data handler of the server time
  *
  *@apiSuccessExample Success Response:
  *{
  *"result": "success",
  *"server_time":"2018-05-20 13:41:45"
  *}
  *
  *@apiError (FailResponse) {String} result Handle the server response.
  *
  *@apiErrorExample Fail Response:
  *{
  *  "result":"fail"
  *}
  */
  public function getServerTime(){
    header('Access-Control-Allow-Origin: *');
    die(json_encode(array("result" => "success", "server_time" => date("Y-m-d H:i:s"))));
  }
}