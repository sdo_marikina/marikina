<?php
/**
 * ajax.php
 * Ajax controller
 * @package NTEK MVC
 * @author Norielle Cruz <nogats07@gmail.com>
 */

class Ajax_Controller extends MVC_Controller {
  public function index(){
    header('Access-Control-Allow-Origin: *');
    die(json_encode(array("result" => "fail")));
  }

  public function login(){
    header('Access-Control-Allow-Origin: *');
    if(isset($_SESSION["logged"]))
      die(json_encode(array("result" => "fail", "message" => "you are trying to mess up the system!")));
    $data = $_POST;

    if(!isset($data["username"], $data["password"]))
      die(json_encode(array("result" => "error", "message" => "Something went wrong while processing your request!")));

    if($this->system->checkUser($data["username"]) > 0){
      if($this->system->checkPassword($data)){
        $_SESSION["logged"] = $this->system->getUser($data["username"]);
        die(json_encode(array("result" => "success", "message" => "Successfully logged in!")));
      } else {
        die(json_encode(array("result" => "fail", "message" => "Password is incorrect!")));
      }
    } else {
      die(json_encode(array("result" => "fail", "message" => "User was not found!")));
    }
  }

  public function dtr(){
    header('Access-Control-Allow-Origin: *');
    if(!isset($_SESSION["logged"]))
      die(json_encode(array("result" => "fail", "message" => "you are trying to mess up the system!")));

    $data = $_POST;
    if(!isset($data["start"], $data["end"], $data["department"], $data["type"], $data["weekends"]))
      die(json_encode(array("result" => "fail", "message" => "Some fields are not set!")));

    $settings = $this->system->getSettings();
    $employees = $this->system->getReportEmployees($data);
    $holidays = $this->system->getHolidaysDTR();
    if($data["weekends"] == 1){
      $dates = date_range($data["start"], $data["end"], "+1 day", "Y-m-d", true);
    } else {
      $dates = date_range($data["start"], $data["end"], "+1 day", "Y-m-d");
    }

    $dateStart = $data["start"];
    $dateEnd = $data["end"];

    $data = "";
    $data .= "SDO-Marikina\n";
    $data .= "DAILY TIME RECORD\n";
    $data .= "From: {$dateStart} To: {$dateEnd}\n\n\n";

    if(!empty($employees)){
      foreach($employees as $employee){
        $schedule = explode(",", $this->system->getEmployeeSchedule($employee["eid"])["schedule"]);
        $baseTime = abs((strtotime($schedule[0]) - strtotime($schedule[1])) / 3600);
        $baseAM = abs((strtotime($schedule[0]) - strtotime($schedule[2])) / 3600); // Only for teaching and non-teaching
        $basePM = abs((strtotime($schedule[3]) - strtotime($schedule[1])) / 3600); // Only for teaching and non-teaching
        $data .= "Employee: {$employee["lname"]}, {$employee["fname"]} ".substr($employee["mname"], 0, 1).". (".$employee["eid"].")\n";
        if($employee["type"] == 3){
          /**
           * Division Personnel Computation
           */

          $data .= "Schedule: ".date("g:i a", strtotime($schedule[0]))." - ".date("g:i a", strtotime($schedule[1]))."\n";
          $data .= "Break: ".date("g:i a", strtotime($schedule[2]))." - ".date("g:i a", strtotime($schedule[3]))."\n";
          $data .= "Date,In 1,Out 1,In 2,Out 2,Total Break,Hours Worked (AM),Hours Worked (PM),Total Hours Worked,Undertime\n";
          foreach($dates as $date){
            if(!in_array(date("m-d", strtotime($date)), $holidays)){
              $access = $this->system->getTimeRecord(array("eid" => $employee["eid"], "date" => $date));

              if(!empty($access) && count($access) >= 4){
                // do the math
                $hourIn = date("g", strtotime($access[0]["time"]));
                $minuteIn = date("i", strtotime($access[0]["time"]));
                switch($hourIn){
                  case 7:
                    $inTime = "{$hourIn}:{$minuteIn} AM";
                    $outTime = "4:{$minuteIn} PM";
                    break;
                  case 8:
                    $inTime = "{$hourIn}:{$minuteIn} AM";
                    $outTime = "5:{$minuteIn} PM";
                    break;
                  case 9:
                    $inTime = "{$hourIn}:{$minuteIn} AM";
                    if($minuteIn > 30){
                      $outTime = "6:30 PM";
                    } else {
                      $outTime = "6:{$minuteIn} PM";
                    }
                    break;
                  default:
                    $outTime = "6:30 PM";
                    if($hourIn < 7){
                      $inTime = "7:00 AM";
                      $outTime = "4:00 PM";
                    } else {
                      $inTime = "9:30 AM";
                    }
                }
                // separated base time for flexitime
                $baseAM = abs((strtotime(($minuteIn > 30 ? "9:30 AM" : $inTime)) - strtotime($schedule[2])) / 3600);
                $basePM = abs((strtotime($schedule[3]) - strtotime($outTime)) / 3600);
                $break = 60 - abs((((strtotime($schedule[2]) - strtotime($access[1]["time"])) / 60) > 0 ? 0 : ((strtotime($schedule[2]) - strtotime($access[1]["time"])) / 60)) + (((strtotime($access[2]["time"]) - strtotime($schedule[3])) / 60) > 0 ? 0 : ((strtotime($access[2]["time"]) - strtotime($schedule[3])) / 60)));
                $underAM = abs((((strtotime($access[1]["time"]) - strtotime($schedule[2])) / 60) > 0 ? 0 : (strtotime($access[1]["time"]) - strtotime($schedule[2])) / 60)) + abs((((strtotime($access[0]["time"]) - strtotime(($minuteIn > 30 ? "9:30 AM" : $inTime))) / 60) < 0 ? 0 : (strtotime($access[0]["time"]) - strtotime(($minuteIn > 30 ? "9:30 AM" : $inTime))) / 60));
                $underPM = abs((((strtotime($schedule[3]) - strtotime($access[2]["time"])) / 60) > 0 ? 0 : ((strtotime($schedule[3]) - strtotime($access[2]["time"])) / 60))) + abs((((strtotime($outTime) - strtotime($access[3]["time"])) / 60) < 0 ? 0 : ((strtotime($outTime) - strtotime($access[3]["time"])) / 60)));
                $hwAm = ($baseAM * 60) - $underAM;
                $hwPm = ($basePM * 60) - $underPM;
                $hwTotal = round(abs(($hwAm + $hwPm) / 24 * 24), 2);
                $underTime = array($underAM, $underPM);
                $lateMinutes[] = array_sum(array(
                  abs(((strtotime($access[0]["time"]) - strtotime($inTime)) / 60) < 0 ? 0 : (strtotime($access[0]["time"]) - strtotime($inTime)) / 60), 
                  abs(((strtotime($access[2]["time"]) - strtotime($schedule[3])) / 60) < 0 ? 0 : (strtotime($access[2]["time"]) - strtotime($schedule[3])) / 60)
                ));
                $lateHours[] = (array_sum($lateMinutes) / 60);
                $totalUnderTime[] = array_sum($underTime);
                $totalAbsent[] = 0;

                // display computed data by date
                $data .= date("m/d/Y", strtotime($date)).",";
                $data .= date("g:i a", strtotime($access[0]["time"])).manualMark($access[0]).",".date("g:i a", strtotime($access[1]["time"])).manualMark($access[1]).",".date("g:i a", strtotime($access[2]["time"])).manualMark($access[2]).",".date("g:i a", strtotime($access[3]["time"])).manualMark($access[3]).",".$break.",".$hwAm.",".$hwPm.",".$hwTotal.",".round(abs(array_sum($underTime)), 2)."\n";
              } else {
                $lateMinutes[] = 0;
                $lateHours[] = 0;
                $totalUnderTime[] = 0;
                $totalAbsent[] = array_sum(array(($baseTime * 60) / 2, ($baseTime * 60) / 2));
                $data .= date("m/d/Y", strtotime($date)).",";
                $data .= (!empty($access[0]["time"]) ? date("g:i a", strtotime($access[0]["time"])).manualMark($access[0]) : "").",".(!empty($access[1]["time"]) ? date("g:i a", strtotime($access[1]["time"])).manualMark($access[1]) : "").",".(!empty($access[2]["time"]) ? date("g:i a", strtotime($access[2]["time"])).manualMark($access[2]) : "").",".(!empty($access[3]["time"]) ? date("g:i a", strtotime($access[3]["time"])).manualMark($access[3]) : "").", , , , , \n";
              }

              $date_raw[] = array(
                  "date" => date("m/d/Y", strtotime($date)),
                  "in1" => (empty($access[0]["time"]) ? "" : date("g:i a", strtotime($access[0]["time"])).manualMark($access[0])),
                  "out1" => (empty($access[1]["time"]) ? "" : date("g:i a", strtotime($access[1]["time"])).manualMark($access[1])),
                  "in2" => (empty($access[2]["time"]) ? "" : date("g:i a", strtotime($access[2]["time"])).manualMark($access[2])),
                  "out2" => (empty($access[3]["time"]) ? "" : date("g:i a", strtotime($access[3]["time"])).manualMark($access[3])),
                  "break" => (!isset($break) ? "" : $break),
                  "hwAm" => (!isset($hwAm) ? "" : $hwAm),
                  "hwPm" => (!isset($hwPm) ? "" : $hwPm),
                  "hwTotal" => (!isset($hwTotal) ? "" : $hwTotal),
                  "underTime" => (!isset($underTime) ? "" : round(abs(array_sum($underTime)), 2))
              );
              
              unset($break, $hwAm, $hwPm, $hwTotal, $underTime);
            }
          }
        } else if($employee["type"] == 2){
          /**
           * Teaching Computation
           */

          $data .= "Schedule: ".date("g:i a", strtotime($schedule[0]))." - ".date("g:i a", strtotime($schedule[1]))."\n";
          $data .= "Break: ".date("g:i a", strtotime($schedule[2]))." - ".date("g:i a", strtotime($schedule[3]))."\n";
          $data .= "Date,In,Out,Total Hours Worked,Undertime\n";

          foreach($dates as $key => $date){
            if(!in_array(date("m-d", strtotime($date)), $holidays)){
              $access = $this->system->getTimeRecord(array("eid" => $employee["eid"], "date" => $date));

              if(!empty($access) && count($access) >= 2){
                // do the math
                $inUnder = ((((strtotime($access[0]["time"]) - strtotime($schedule[0])) / 60) / 24 * 24) < 0 ? 0 : ((strtotime($access[0]["time"]) - strtotime($schedule[0])) / 60) / 24 * 24);
                $outUnder = ((((strtotime($access[1]["time"]) - strtotime($schedule[1])) / 60) / 24 * 24) > 0 ? 0 : round(abs(((strtotime($access[1]["time"]) - strtotime($schedule[1])) / 60) / 24 * 24), 2));
                $hwTotal = round(abs((($baseTime * 60) - ($inUnder + $outUnder))), 2);
                $underTime = $inUnder + $outUnder;
                $lateMinutes[] = $inUnder;
                $lateHours[] = array_sum($lateMinutes) / 60;
                $totalUnderTime[] = $underTime;
                $totalAbsent[] = 0;

                // computed data by date
                $data .= date("m/d/Y", strtotime($date)).",";
                $data .= date("g:i a", strtotime($access[0]["time"])).manualMark($access[0]).",".date("g:i a", strtotime($access[1]["time"])).manualMark($access[1]).",".$hwTotal.",".round(abs($underTime), 2)."\n";
              } else {
                $lateMinutes[] = 0;
                $lateHours[] = 0;
                $totalUnderTime[] = 0;
                $totalAbsent[] = array_sum(array(($baseTime * 60)));
                $data .= date("m/d/Y", strtotime($date)).",";
                $data .= (!empty($access[0]["time"]) ? date("g:i a", strtotime($access[0]["time"])).manualMark($access[0]) : "").",".(!empty($access[1]["time"]) ? date("g:i a", strtotime($access[1]["time"])).manualMark($access[1]) : "").", , \n";
              }

              $date_raw[] = array(
                  "date" => date("m/d/Y", strtotime($date)),
                  "in" => (empty($access[0]["time"]) ? "" : date("g:i a", strtotime($access[0]["time"])).manualMark($access[0])),
                  "out" => (empty($access[1]["time"]) ? "" : date("g:i a", strtotime($access[1]["time"])).manualMark($access[1])),
                  "hwTotal" => (!isset($hwTotal) ? "" : $hwTotal),
                  "underTime" => (!isset($underTime) ? "" : round(abs($underTime), 2))
              );
              unset($break, $hwAm, $hwPm, $hwTotal, $underTime);
            }
          }

          $data .= ", , , , , , , ,Total UT Minutes: ".round(abs(array_sum($totalUnderTime)), 2)."\n";
          $data .= ", , , , , , , ,Total UT Days: ".round(abs(array_sum($totalUnderTime) / 60 / 24), 2)."\n";
          $data .= ", , , , , , , ,Total Days Absent: ".round(abs(array_sum($totalAbsent) / 60 / $baseTime), 0)."\n";
          $data .= ", , , , , , , ,Total Minutes Late: ".abs(array_sum($lateMinutes))."\n";
          $data .= ", , , , , , , ,Total Hours Late: ".round(abs(array_sum($lateHours)), 2)."\n";
        } else {
          /**
           * Non-Teaching Computation
           */

          $data .= "Schedule: ".date("g:i a", strtotime($schedule[0]))." - ".date("g:i a", strtotime($schedule[1]))."\n";
          $data .= "Break: ".date("g:i a", strtotime($schedule[2]))." - ".date("g:i a", strtotime($schedule[3]))."\n";
          $data .= "Date,In 1,Out 1,In 2,Out 2,Total Break,Hours Worked (AM),Hours Worked (PM),Total Hours Worked,Undertime\n";
          foreach($dates as $key => $date){
            if(!in_array(date("m-d", strtotime($date)), $holidays)){
              $access = $this->system->getTimeRecord(array("eid" => $employee["eid"], "date" => $date));

              if(!empty($access) && count($access) >= 4){
                // do the math
                $break = 60 - abs((((strtotime($schedule[2]) - strtotime($access[1]["time"])) / 60) > 0 ? 0 : ((strtotime($schedule[2]) - strtotime($access[1]["time"])) / 60)) + (((strtotime($access[2]["time"]) - strtotime($schedule[3])) / 60) > 0 ? 0 : ((strtotime($access[2]["time"]) - strtotime($schedule[3])) / 60)));
                $underAM = abs(((strtotime($access[1]["time"]) - strtotime($schedule[2])) / 60) > 0 ? 0 : (strtotime($access[1]["time"]) - strtotime($schedule[2])) / 60) + abs(((strtotime($access[0]["time"]) - strtotime("+{$settings["graceperiod"]} minutes", strtotime($schedule[0]))) / 60) < 0 ? 0 : (strtotime($access[0]["time"]) - strtotime("+{$settings["graceperiod"]} minutes", strtotime($schedule[0]))) / 60);
                $underPM = (((strtotime($access[2]["time"]) - strtotime($schedule[3])) / 60) < 0 ? 0 : ((strtotime($access[2]["time"]) - strtotime($schedule[3])) / 60)) + (((strtotime($schedule[1]) - strtotime($access[3]["time"])) / 60) < 0 ? 0 : ((strtotime($schedule[1]) - strtotime($access[3]["time"])) / 60));
                $hwAm = ($baseAM * 60) - $underAM;
                $hwPm = ($basePM * 60) - $underPM;
                $hwTotal = round(abs(($hwAm + $hwPm) / 24 * 24), 2);
                $underTime = array($underAM, $underPM);
                $lateMinutes[] = array_sum(array(
                  abs(((strtotime($access[0]["time"]) - strtotime("+{$settings["graceperiod"]} minutes", strtotime($schedule[0]))) / 60) < 0 ? 0 : (strtotime($access[0]["time"]) - strtotime("+{$settings["graceperiod"]} minutes", strtotime($schedule[0]))) / 60), 
                  abs(((strtotime($access[2]["time"]) - strtotime($schedule[3])) / 60) < 0 ? 0 : (strtotime($access[2]["time"]) - strtotime($schedule[3])) / 60)
                ));
                $lateHours[] = (array_sum($lateMinutes) / 60);
                $totalUnderTime[] = array_sum($underTime);
                $totalAbsent[] = 0;

                // display computed data by date
                $data .= date("m/d/Y", strtotime($date)).",";
                $data .= date("g:i a", strtotime($access[0]["time"])).manualMark($access[0]).",".date("g:i a", strtotime($access[1]["time"])).manualMark($access[1]).",".date("g:i a", strtotime($access[2]["time"])).manualMark($access[2]).",".date("g:i a", strtotime($access[3]["time"])).manualMark($access[3]).",".$break.",".$hwAm.",".$hwPm.",".$hwTotal.",".round(abs(array_sum($underTime)), 2)."\n";
              } else {
                $lateMinutes[] = 0;
                $lateHours[] = 0;
                $totalUnderTime[] = 0;
                $totalAbsent[] = array_sum(array(($baseTime * 60) / 2, ($baseTime * 60) / 2));
                $data .= date("m/d/Y", strtotime($date)).",";
                $data .= (!empty($access[0]["time"]) ? date("g:i a", strtotime($access[0]["time"])).manualMark($access[0]) : "").",".(!empty($access[1]["time"]) ? date("g:i a", strtotime($access[1]["time"])).manualMark($access[1]) : "").",".(!empty($access[2]["time"]) ? date("g:i a", strtotime($access[2]["time"])).manualMark($access[2]) : "").",".(!empty($access[3]["time"]) ? date("g:i a", strtotime($access[3]["time"])).manualMark($access[3]) : "").", , , , , \n";
              }

              $date_raw[] = array(
                  "date" => date("m/d/Y", strtotime($date)),
                  "in1" => (empty($access[0]["time"]) ? "" : date("g:i a", strtotime($access[0]["time"])).manualMark($access[0])),
                  "out1" => (empty($access[1]["time"]) ? "" : date("g:i a", strtotime($access[1]["time"])).manualMark($access[1])),
                  "in2" => (empty($access[2]["time"]) ? "" : date("g:i a", strtotime($access[2]["time"])).manualMark($access[2])),
                  "out2" => (empty($access[3]["time"]) ? "" : date("g:i a", strtotime($access[3]["time"])).manualMark($access[3])),
                  "break" => (!isset($break) ? "" : $break),
                  "hwAm" => (!isset($hwAm) ? "" : $hwAm),
                  "hwPm" => (!isset($hwPm) ? "" : $hwPm),
                  "hwTotal" => (!isset($hwTotal) ? "" : $hwTotal),
                  "underTime" => (!isset($underTime) ? "" : round(abs(array_sum($underTime)), 2))
              );

              unset($break, $hwAm, $hwPm, $hwTotal, $underTime);
            }
          }

          $data .= ", , , , , , , ,Total UT Minutes: ".round(abs(array_sum($totalUnderTime)), 0)."\n";
          $data .= ", , , , , , , ,Total UT Days: ".round(abs(array_sum($totalUnderTime) / 60 / 24), 2)."\n";
          $data .= ", , , , , , , ,Total Days Absent: ".round(abs(array_sum($totalAbsent) / 60 / $baseTime), 0)."\n";
          $data .= ", , , , , , , ,Total Minutes Late: ".round(abs(array_sum($lateMinutes)), 0)."\n";
          $data .= ", , , , , , , ,Total Hours Late: ".round(abs(array_sum($lateHours)), 2)."\n";
        }
        unset($lateMinutes, $lateHours, $totalUnderTime, $totalAbsent);
        $data .= "\n\n";
        $raw[$employee["eid"]] = array(
          "name" => trim("{$employee["lname"]}, {$employee["fname"]} ".substr($employee["mname"], 0, 1).". "),
          "eid" => $employee["eid"],
          "type" => $employee["type"],
          "date_start" => $dateStart,
          "date_end" => $dateEnd,
          "attendance" => $date_raw
        );
        $date_raw = array();
      }
    } else {
      die(json_encode(array("result" => "fail", "message" => "No employees were found!")));
    }

    $settings = $this->system->getSettings();
    $fields = array(
      "report" => $data,
      "raw" => json_encode($raw),
      "client_key" => $settings["key"],
      "client_secret" => $settings["secret"]
    );
    $fields_string = "";
    foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
    rtrim($fields_string, '&');
    $remote = curl_init();
    curl_setopt($remote, CURLOPT_URL, "{$settings["remote_url"]}/api/dtr");
    curl_setopt($remote, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($remote, CURLOPT_POST, count($fields));
    curl_setopt($remote, CURLOPT_POSTFIELDS, $fields_string);
    $response = curl_exec($remote);
    curl_close($remote);
    if(!empty($response)){
      die($response);
    } else {
      die(json_encode(array("result" => "fail", "message" => "Something went wrong!")));
    }
  }

  public function form7(){
    header('Access-Control-Allow-Origin: *');
    if(!isset($_SESSION["logged"]))
      die(json_encode(array("result" => "fail", "message" => "you are trying to mess up the system!")));

    $data = $_POST;
    if(!isset($data["start"], $data["end"], $data["department"], $data["type"], $data["weekends"]))
      die(json_encode(array("result" => "fail", "message" => "Some fields are not set!")));

    $settings = $this->system->getSettings();
    $employees = $this->system->getReportEmployees($data);
    $holidays = $this->system->getHolidaysDTR();
    if($data["weekends"] == 1){
      $dates = date_range($data["start"], $data["end"], "+1 day", "Y-m-d", true);
    } else {
      $dates = date_range($data["start"], $data["end"], "+1 day", "Y-m-d");
    }

    $baseCount = 22;
    $baseAbsent = 22;

    if(!empty($employees)){
      foreach($employees as $employee){
        if($employee["schedule"] !== NULL){
          $schedule = explode(",", $this->system->getEmployeeSchedule($employee["eid"])["schedule"]);
          $baseTime = abs((strtotime($schedule[0]) - strtotime($schedule[1])) / 3600);
          $baseAM = abs((strtotime($schedule[0]) - strtotime($schedule[2])) / 3600); // Only for teaching and non-teaching
          $basePM = abs((strtotime($schedule[3]) - strtotime($schedule[1])) / 3600); // Only for teaching and non-teaching

          if($employee["type"] == 3){
            foreach($dates as $key => $date){
              if(!in_array(date("m-d", strtotime($date)), $holidays)){
                $access = $this->system->getTimeRecord(array("eid" => $employee["eid"], "date" => $date));
                if(!empty($access) && count($access) >= 4){
                  $hourIn = date("g", strtotime($access[0]["time"]));
                  $minuteIn = date("i", strtotime($access[0]["time"]));
                  switch($hourIn){
                    case 7:
                      $inTime = "{$hourIn}:{$minuteIn} AM";
                      $outTime = "4:{$minuteIn} PM";
                      break;
                    case 8:
                      $inTime = "{$hourIn}:{$minuteIn} AM";
                      $outTime = "5:{$minuteIn} PM";
                      break;
                    case 9:
                      $inTime = "{$hourIn}:{$minuteIn} AM";
                      if($minuteIn > 30){
                        $outTime = "6:30 PM";
                      } else {
                        $outTime = "6:{$minuteIn} PM";
                      }
                      break;
                    default:
                      $outTime = "6:30 PM";
                      if($hourIn < 7){
                        $inTime = "7:00 AM";
                        $outTime = "4:00 PM";
                      } else {
                        $inTime = "9:30 AM";
                      }
                  }
                  // separated base time for flexitime
                  $baseAM = abs((strtotime(($minuteIn > 30 ? "9:30 AM" : $inTime)) - strtotime($schedule[2])) / 3600);
                  $basePM = abs((strtotime($schedule[3]) - strtotime($outTime)) / 3600);
                  $break = 60 - abs((((strtotime($schedule[2]) - strtotime($access[1]["time"])) / 60) > 0 ? 0 : ((strtotime($schedule[2]) - strtotime($access[1]["time"])) / 60)) + (((strtotime($access[2]["time"]) - strtotime($schedule[3])) / 60) > 0 ? 0 : ((strtotime($access[2]["time"]) - strtotime($schedule[3])) / 60)));
                  $underAM = abs((((strtotime($access[1]["time"]) - strtotime($schedule[2])) / 60) > 0 ? 0 : (strtotime($access[1]["time"]) - strtotime($schedule[2])) / 60)) + abs((((strtotime($access[0]["time"]) - strtotime(($minuteIn > 30 ? "9:30 AM" : $inTime))) / 60) < 0 ? 0 : (strtotime($access[0]["time"]) - strtotime(($minuteIn > 30 ? "9:30 AM" : $inTime))) / 60));
                  $underPM = abs((((strtotime($schedule[3]) - strtotime($access[2]["time"])) / 60) > 0 ? 0 : ((strtotime($schedule[3]) - strtotime($access[2]["time"])) / 60))) + abs((((strtotime($outTime) - strtotime($access[3]["time"])) / 60) < 0 ? 0 : ((strtotime($outTime) - strtotime($access[3]["time"])) / 60)));
                  $hwAm = ($baseAM * 60) - $underAM;
                  $hwPm = ($basePM * 60) - $underPM;
                  $hwTotal = round(abs(($hwAm + $hwPm) / 24 * 24), 2);
                  $underTime = array($underAM, $underPM);
                  $lateMinutes[] = array_sum(array(
                    abs(((strtotime($access[0]["time"]) - strtotime($inTime)) / 60) < 0 ? 0 : (strtotime($access[0]["time"]) - strtotime($inTime)) / 60), 
                    abs(((strtotime($access[2]["time"]) - strtotime($schedule[3])) / 60) < 0 ? 0 : (strtotime($access[2]["time"]) - strtotime($schedule[3])) / 60)
                  ));
                  $lateHours[] = (array_sum($lateMinutes) / 60);
                  $totalUnderTime[] = array_sum($underTime);
                } else {
                  $lateMinutes[] = 0;
                  $lateHours[] = 0;
                  $totalUnderTime[] = 0;
                  $baseAbsent--;
                }
              }
            }
          } else if($employee["type"] == 2){
            foreach($dates as $key => $date){
              if(!in_array(date("m-d", strtotime($date)), $holidays)){
                $access = $this->system->getTimeRecord(array("eid" => $employee["eid"], "date" => $date));
                if(!empty($access) && count($access) >= 2){
                  // do the math
                  $inUnder = ((((strtotime($access[0]["time"]) - strtotime($schedule[0])) / 60) / 24 * 24) < 0 ? 0 : ((strtotime($access[0]["time"]) - strtotime($schedule[0])) / 60) / 24 * 24);
                  $outUnder = ((((strtotime($access[1]["time"]) - strtotime($schedule[1])) / 60) / 24 * 24) > 0 ? 0 : round(abs(((strtotime($access[1]["time"]) - strtotime($schedule[1])) / 60) / 24 * 24), 2));
                  $hwTotal = round(abs((($baseTime * 60) - ($inUnder + $outUnder))), 2);
                  $underTime = $inUnder + $outUnder;
                  $lateMinutes[] = $inUnder;
                  $lateHours[] = array_sum($lateMinutes) / 60;
                  $totalUnderTime[] = $underTime;
                } else {
                  $lateMinutes[] = 0;
                  $lateHours[] = 0;
                  $totalUnderTime[] = 0;
                  $baseAbsent--;
                }
              }
            }
          } else {
            foreach($dates as $key => $date){
              if(!in_array(date("m-d", strtotime($date)), $holidays)){
                $access = $this->system->getTimeRecord(array("eid" => $employee["eid"], "date" => $date));
                if(!empty($access) && count($access) >= 4){
                  // do the math
                  $break = 60 - abs((((strtotime($schedule[2]) - strtotime($access[1]["time"])) / 60) > 0 ? 0 : ((strtotime($schedule[2]) - strtotime($access[1]["time"])) / 60)) + (((strtotime($access[2]["time"]) - strtotime($schedule[3])) / 60) > 0 ? 0 : ((strtotime($access[2]["time"]) - strtotime($schedule[3])) / 60)));
                  $underAM = abs(((strtotime($access[1]["time"]) - strtotime($schedule[2])) / 60) > 0 ? 0 : (strtotime($access[1]["time"]) - strtotime($schedule[2])) / 60) + abs(((strtotime($access[0]["time"]) - strtotime("+{$settings["graceperiod"]} minutes", strtotime($schedule[0]))) / 60) < 0 ? 0 : (strtotime($access[0]["time"]) - strtotime("+{$settings["graceperiod"]} minutes", strtotime($schedule[0]))) / 60);
                  $underPM = (((strtotime($access[2]["time"]) - strtotime($schedule[3])) / 60) < 0 ? 0 : ((strtotime($access[2]["time"]) - strtotime($schedule[3])) / 60)) + (((strtotime($schedule[1]) - strtotime($access[3]["time"])) / 60) < 0 ? 0 : ((strtotime($schedule[1]) - strtotime($access[3]["time"])) / 60));
                  $hwAm = ($baseAM * 60) - $underAM;
                  $hwPm = ($basePM * 60) - $underPM;
                  $hwTotal = round(abs(($hwAm + $hwPm) / 24 * 24), 2);
                  $underTime = array($underAM, $underPM);
                  $lateMinutes[] = array_sum(array(
                    abs(((strtotime($access[0]["time"]) - strtotime("+{$settings["graceperiod"]} minutes", strtotime($schedule[0]))) / 60) < 0 ? 0 : (strtotime($access[0]["time"]) - strtotime("+{$settings["graceperiod"]} minutes", strtotime($schedule[0]))) / 60), 
                    abs(((strtotime($access[2]["time"]) - strtotime($schedule[3])) / 60) < 0 ? 0 : (strtotime($access[2]["time"]) - strtotime($schedule[3])) / 60)
                  ));
                  $lateHours[] = (array_sum($lateMinutes) / 60);
                  $totalUnderTime[] = array_sum($underTime);
                } else {
                  $lateMinutes[] = 0;
                  $lateHours[] = 0;
                  $totalUnderTime[] = 0;
                  $baseAbsent--;
                }
              }
            }
          }
          
          $absent = abs(22 - $baseAbsent);
          $hrs_late = round(abs(array_sum($lateHours)), 2);
          $mins_late = sprintf('%0.2f', abs(array_sum($totalUnderTime)));
        	$rate = array(
        		"per_mo" => $employee["basic_pay"] / $baseCount,
              "per_hr" => (($employee["basic_pay"] / $baseCount) / 480) / 60,
              "per_min" => ($employee["basic_pay"] / $baseCount) / 480
        	);

        	$pera = array(
        		"pera_mo" => 2000 / $baseCount,
              "pera_hr" => ((2000 / $baseCount) / 480) / 60,
              "pera_min" => (2000 / $baseCount) / 480
        	);

          $form7[$employee["eid"]] = array(
            "employee_name" => $employee["lname"].", ".$employee["fname"]." ".substr($employee["mname"], 0, 1).".",
            "employee_no" => $employee["eid"],
            "basic" => $employee["basic_pay"],
            "no_days" => $baseCount,
            "no_absent" => $absent,
            "no_hrs_late" => $hrs_late,
            "no_mins_late" => $mins_late,
            "rate" => array(
              "per_mo" => $rate["per_mo"],
              "per_hr" => $rate["per_hr"],
              "per_min" => $rate["per_min"],
              "basic" => ($rate["per_mo"] * $absent) + ($rate["per_min"] * $mins_late)
            ),
            "deductions" => array(
              "pera_mo" => $pera["pera_mo"],
              "pera_hr" => $pera["pera_hr"],
              "pera_min" => $pera["pera_min"],
              "pera" => ($pera["pera_mo"] * $absent) + ($pera["pera_min"] * $mins_late)
            ),
            "total" => round((($rate["per_mo"] * $absent) + ($rate["per_min"] * $mins_late)) + ($pera["pera_mo"] * $absent) + ($pera["pera_min"] * $mins_late), 2)
          );
          $baseAbsent = 22;
          unset($lateMinutes, $lateHours, $totalUnderTime);
        }
      }
    } else {
      die(json_encode(array("result" => "fail", "message" => "No employees were found!")));
    }

    if(empty($form7)){
      die(json_encode(array("result" => "fail", "message" => "No employees with deductions found!")));
    }

    $settings = $this->system->getSettings();
    $fields = array(
      "form7" => json_encode($form7),
      "client_key" => $settings["key"],
      "client_secret" => $settings["secret"]
    );
    $fields_string = "";
    foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
    rtrim($fields_string, '&');
    $remote = curl_init();
    curl_setopt($remote, CURLOPT_URL, "{$settings["remote_url"]}/api/form7");
    curl_setopt($remote, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($remote, CURLOPT_POST, count($fields));
    curl_setopt($remote, CURLOPT_POSTFIELDS, $fields_string);
    $response = curl_exec($remote);
    curl_close($remote);
    if(!empty($response)){
      die($response);
    } else {
      die(json_encode(array("result" => "fail", "message" => "Something went wrong!")));
    }
  }

  public function attendance(){
    header('Access-Control-Allow-Origin: *');
    if(!isset($_SESSION["logged"]))
      die(json_encode(array("data" => NULL)));

    if(file_exists("system/data/attendance.log")){
      $attendance = explode("\n", trim(file_get_contents("system/data/attendance.log")));
      if(!empty($attendance)){
        $count = 1;
        foreach($attendance AS $log){
          $raw = explode("<=>", $log);
          $data[] = array("count" => $count, "eid" => $raw[0], "name" => ucwords($raw[1]), "access" => $raw[2], "date" => $raw[3]);
          $count++;
        }
        cleanLog("system/data/attendance.log", 100);
      } else {
        $data = array();
      }
    } else {
      $data = array();
    } 

    die(json_encode(array("data" => $data)));
  }

  public function history(){
    header('Access-Control-Allow-Origin: *');
    if(!isset($_SESSION["logged"]))
      die(json_encode(array("data" => NULL)));
    else
      die(json_encode(array("data" => $this->system->getAttendance())));
  }

  public function employees(){
    header('Access-Control-Allow-Origin: *');
    if(!isset($_SESSION["logged"]))
      die(json_encode(array("data" => NULL)));
    else
      die(json_encode(array("data" => $this->system->getEmployees())));
  }

  public function positions(){
    header('Access-Control-Allow-Origin: *');
    if(!isset($_SESSION["logged"]))
      die(json_encode(array("data" => NULL)));
    else
      die(json_encode(array("data" => $this->system->getPositions())));
  }

  public function departments(){
    header('Access-Control-Allow-Origin: *');
    if(!isset($_SESSION["logged"]))
      die(json_encode(array("data" => NULL)));
    else
      die(json_encode(array("data" => $this->system->getDepartments())));
  }

  public function holidays(){
    header('Access-Control-Allow-Origin: *');
    if(!isset($_SESSION["logged"]))
      die(json_encode(array("data" => NULL)));
    else
      die(json_encode(array("data" => $this->system->getHolidays())));
  }

  public function accounts(){
    header('Access-Control-Allow-Origin: *');
    if(!isset($_SESSION["logged"]))
      die(json_encode(array("data" => NULL)));
    else
      die(json_encode(array("data" => $this->system->getAccounts())));
  }

  public function settings(){
    header('Access-Control-Allow-Origin: *');
    if(!isset($_SESSION["logged"]))
      die(json_encode(array("result" => "fail", "message" => "you are trying to mess up the system!")));

    $data = $_POST;
    if(empty($data)){
      die(json_encode(array("result" => "fail", "message" => "Some fields are not set!")));
    }

    if($this->system->saveSettings($data))
      die(json_encode(array("result" => "success", "message" => "Settings has been successfully saved!")));
    else 
      die(json_encode(array("result" => "fail", "message" => "Something went wrong!")));
  }

  public function add_employee(){
    header('Access-Control-Allow-Origin: *');
    if(!isset($_SESSION["logged"]))
      die(json_encode(array("result" => "fail", "message" => "You are trying to mess up the system!")));

    $data = $_POST;
    if(!isset($data["eid"], $data["fname"], $data["mname"], $data["lname"], $data["basic"], $data["department"], $data["type"], $data["tin"], $data["tout"], $data["bin"], $data["bout"])){
      die(json_encode(array("result" => "fail", "message" => "Some fields are not set!")));
    }

    if(empty($data["eid"])){
      die(json_encode(array("result" => "fail", "message" => "Employee number cannot be empty!")));
    }

    if(!isInt($data["basic"])){
      die(json_encode(array("result" => "fail", "message" => "Invalid basic pay! Only numeric value is allowed!")));
    }

    $data["schedule"] = "{$data["tin"]},{$data["tout"]},{$data["bin"]},{$data["bout"]}";

    if($this->system->checkEmployeeNumber($data["eid"]) > 0){
      die(json_encode(array("result" => "fail", "message" => "Employee number is already used by another employee!")));
    } else {
      if($this->system->addEmployee($data)){
        die(json_encode(array("result" => "success", "message" => "Employee has been successfully added!")));
      } else {
        die(json_encode(array("result" => "fail", "message" => "Something went wrong!")));
      }
    }
  }

  public function add_position(){
    header('Access-Control-Allow-Origin: *');
    if(!isset($_SESSION["logged"]))
      die(json_encode(array("result" => "fail", "message" => "You are trying to mess up the system!")));

    $data = $_POST;
    if(!isset($data["name"], $data["description"])){
      die(json_encode(array("result" => "fail", "message" => "Some fields are not set!")));
    }

    if($this->system->addPosition($data)){
      die(json_encode(array("result" => "success", "message" => "Position has been successfully added!")));
    } else {
      die(json_encode(array("result" => "fail", "message" => "Something went wrong!")));
    }
  }

  public function add_department(){
    header('Access-Control-Allow-Origin: *');
    if(!isset($_SESSION["logged"]))
      die(json_encode(array("result" => "fail", "message" => "You are trying to mess up the system!")));

    $data = $_POST;
    if(!isset($data["name"], $data["description"])){
      die(json_encode(array("result" => "fail", "message" => "Some fields are not set!")));
    }

    if($this->system->addDepartment($data)){
      die(json_encode(array("result" => "success", "message" => "Department has been successfully added!")));
    } else {
      die(json_encode(array("result" => "fail", "message" => "Something went wrong!")));
    }
  }

  public function add_holiday(){
    header('Access-Control-Allow-Origin: *');
    if(!isset($_SESSION["logged"]))
      die(json_encode(array("result" => "fail", "message" => "You are trying to mess up the system!")));

    $data = $_POST;
    if(!isset($data["name"], $data["holiday"])){
      die(json_encode(array("result" => "fail", "message" => "Some fields are not set!")));
    }

    if($this->system->addHoliday($data)){
      die(json_encode(array("result" => "success", "message" => "Holiday has been successfully added!")));
    } else {
      die(json_encode(array("result" => "fail", "message" => "Something went wrong!")));
    }
  }

  public function add_account(){
    header('Access-Control-Allow-Origin: *');
    if(!isset($_SESSION["logged"]))
      die(json_encode(array("result" => "fail", "message" => "You are trying to mess up the system!")));

    $data = $_POST;
    if(!isset($data["username"], $data["password"])){
      die(json_encode(array("result" => "fail", "message" => "Some fields are not set!")));
    }

    if($this->system->checkAccountUsername($data["username"]) > 0){
      die(json_encode(array("result" => "fail", "message" => "Admin username is already taken!")));
    } else {
      if($this->system->addAccount($data)){
        die(json_encode(array("result" => "success", "message" => "Administrator has been successfully added!")));
      } else {
        die(json_encode(array("result" => "fail", "message" => "Something went wrong!")));
      }
    }
  }

  public function edit_employee(){
    header('Access-Control-Allow-Origin: *');
    if(!isset($_SESSION["logged"]))
      die(json_encode(array("result" => "fail", "message" => "This system is hack proof, go die idiot!")));

    $data = $_POST;
    if(!isset($data["id"], $data["eid"], $data["fname"], $data["mname"], $data["lname"], $data["department"], $data["type"], $data["basic"], $data["tin"], $data["tout"], $data["bin"], $data["bout"])){
      die(json_encode(array("result" => "fail", "message" => "Some fields are empty!")));
    }

    if(!isInt($data["department"])){
      die(json_encode(array("result" => "fail", "message" => "You are trying to mess up the system!")));
    }

    if(!isInt($data["type"])){
      die(json_encode(array("result" => "fail", "message" => "You are trying to mess up the system!")));
    }

    if(!isInt($data["basic"])){
      die(json_encode(array("result" => "fail", "message" => "You are trying to mess up the system!")));
    }

    $data["schedule"] = "{$data["tin"]},{$data["tout"]},{$data["bin"]},{$data["bout"]}";

    if($this->system->checkEmployeeNumber($data["eid"]) > 0){
      die(json_encode(array("result" => "fail", "message" => "Employee number is already used by you or another employee!")));
    } else {
      if($this->system->editEmployee($data)){
        die(json_encode(array("result" => "success", "message" => "Employee record has been successfully updated!")));
      } else {
        die(json_encode(array("result" => "fail", "message" => "Something went wrong!")));
      }
    }
  }

  public function edit_position(){
    header('Access-Control-Allow-Origin: *');
    if(!isset($_SESSION["logged"]))
      die(json_encode(array("result" => "fail", "message" => "This system is hack proof, go die idiot!")));

    $data = $_POST;
    if(!isset($data["id"], $data["name"], $data["description"])){
      die(json_encode(array("result" => "fail", "message" => "Some fields are empty!")));
    }

    if($this->system->editPosition($data)){
      die(json_encode(array("result" => "success", "message" => "Position has been successfully updated!")));
    } else {
      die(json_encode(array("result" => "fail", "message" => "Something went wrong!")));
    }
  } 


  public function edit_department(){
    header('Access-Control-Allow-Origin: *');
    if(!isset($_SESSION["logged"]))
      die(json_encode(array("result" => "fail", "message" => "This system is hack proof, go die idiot!")));

    $data = $_POST;
    if(!isset($data["id"], $data["name"], $data["description"])){
      die(json_encode(array("result" => "fail", "message" => "Some fields are empty!")));
    }

    if($this->system->editDepartment($data)){
      die(json_encode(array("result" => "success", "message" => "Department has been successfully updated!")));
    } else {
      die(json_encode(array("result" => "fail", "message" => "Something went wrong!")));
    }
  } 

  public function edit_holiday(){
    header('Access-Control-Allow-Origin: *');
    if(!isset($_SESSION["logged"]))
      die(json_encode(array("result" => "fail", "message" => "This system is hack proof, go die idiot!")));

    $data = $_POST;
    if(!isset($data["id"], $data["name"], $data["holiday"])){
      die(json_encode(array("result" => "fail", "message" => "Some fields are empty!")));
    }

    if($this->system->editHoliday($data)){
      die(json_encode(array("result" => "success", "message" => "Holiday has been successfully updated!")));
    } else {
      die(json_encode(array("result" => "fail", "message" => "Something went wrong!")));
    }
  } 

  public function edit_account(){
    header('Access-Control-Allow-Origin: *');
    if(!isset($_SESSION["logged"]))
      die(json_encode(array("result" => "fail", "message" => "This system is hack proof, go die idiot!")));

    $data = $_POST;
    if(!isset($data["id"], $data["username"], $data["password"])){
      die(json_encode(array("result" => "fail", "message" => "Some fields are empty!")));
    }

    if($this->system->editAccount($data)){
      $_SESSION["logged"] = $this->system->getUser($data["username"]);
      die(json_encode(array("result" => "success", "message" => "Administrator has been successfully updated!")));
    } else {
      die(json_encode(array("result" => "fail", "message" => "Something went wrong!")));
    }
  }  

  public function delete_employee(){
    header('Access-Control-Allow-Origin: *');
    if(!isset($_SESSION["logged"]))
      die(json_encode(array("result" => "fail", "message" => "You are trying to mess up the system!")));

    $data = $_GET;
    if(!isset($data["id"])){
      die(json_encode(array("result" => "fail", "message" => "You are trying to mess up the system!")));
    }

    if(!isInt($data["id"])){
      die(json_encode(array("result" => "fail", "message" => "You are trying to mess up the system!")));
    }

    if($this->system->deleteEmployee($data["id"])){
      @unlink("uploads/fingerprints/{$data["id"]}_template.dat");
      die(json_encode(array("result" => "success", "message" => "Employee record has been deleted!")));
    } else {
      die(json_encode(array("result" => "fail", "message" => "Something went wrong!")));
    }
  }

  public function delete_position(){
    header('Access-Control-Allow-Origin: *');
    if(!isset($_SESSION["logged"]))
      die(json_encode(array("result" => "fail", "message" => "You are trying to mess up the system!")));

    $data = $_GET;
    if(!isset($data["id"])){
      die(json_encode(array("result" => "fail", "message" => "You are trying to mess up the system!")));
    }

    if(!isInt($data["id"])){
      die(json_encode(array("result" => "fail", "message" => "You are trying to mess up the system!")));
    }

    if($this->system->deletePosition($data["id"])){
      die(json_encode(array("result" => "success", "message" => "Position has been deleted!")));
    } else {
      die(json_encode(array("result" => "fail", "message" => "Something went wrong!")));
    }
  }

  public function delete_department(){
    header('Access-Control-Allow-Origin: *');
    if(!isset($_SESSION["logged"]))
      die(json_encode(array("result" => "fail", "message" => "You are trying to mess up the system!")));

    $data = $_GET;
    if(!isset($data["id"])){
      die(json_encode(array("result" => "fail", "message" => "You are trying to mess up the system!")));
    }

    if(!isInt($data["id"])){
      die(json_encode(array("result" => "fail", "message" => "You are trying to mess up the system!")));
    }

    if($this->system->deleteDepartment($data["id"])){
      die(json_encode(array("result" => "success", "message" => "Department has been deleted!")));
    } else {
      die(json_encode(array("result" => "fail", "message" => "Something went wrong!")));
    }
  }

  public function delete_holiday(){
    header('Access-Control-Allow-Origin: *');
    if(!isset($_SESSION["logged"]))
      die(json_encode(array("result" => "fail", "message" => "You are trying to mess up the system!")));

    $data = $_GET;
    if(!isset($data["id"])){
      die(json_encode(array("result" => "fail", "message" => "You are trying to mess up the system!")));
    }

    if(!isInt($data["id"])){
      die(json_encode(array("result" => "fail", "message" => "You are trying to mess up the system!")));
    }

    if($this->system->deleteHoliday($data["id"])){
      die(json_encode(array("result" => "success", "message" => "Holiday has been deleted!")));
    } else {
      die(json_encode(array("result" => "fail", "message" => "Something went wrong!")));
    }
  }

  public function delete_account(){
    header('Access-Control-Allow-Origin: *');
    if(!isset($_SESSION["logged"]))
      die(json_encode(array("result" => "fail", "message" => "You are trying to mess up the system!")));

    $data = $_GET;
    if(!isset($data["id"])){
      die(json_encode(array("result" => "fail", "message" => "You are trying to mess up the system!")));
    }

    if(!isInt($data["id"])){
      die(json_encode(array("result" => "fail", "message" => "You are trying to mess up the system!")));
    }

    if($data["id"] == 1){
      die(json_encode(array("result" => "fail", "message" => "Default admin account cannot be deleted!")));
    }

    if($this->system->deleteAccount($data["id"])){
      if($_SESSION["logged"]["id"] == $data["id"]){
        session_destroy();
      }
      die(json_encode(array("result" => "success", "message" => "Administrator has been deleted!")));
    } else {
      die(json_encode(array("result" => "fail", "message" => "Something went wrong!")));
    }
  }
}