<?php
/**
 * panel.php
 * Panel controller
 * @package NTEK MVC
 * @author Norielle Cruz <nogats07@gmail.com>
 */

class Panel_Controller extends MVC_Controller {
  public function index(){
    $data = array(
      "page" => "panel"
    );

    $this->smarty->assign($data);
    $this->smarty->display("default/header.html");
    $this->smarty->display("default/pages/panel.html");
    $this->smarty->display("default/footer.html");
  }

  public function verify(){
  	header('Access-Control-Allow-Origin: *');
  	$data = $_POST;

  	if(!isset($data["eid"])){
  		die(json_encode(array("result" => "fail", "message" => "Some fields are not set!")));
  	}

  	if($this->panel->verifyEmployeeNumber($data["eid"]) > 0){
  		die(json_encode(array("result" => "success", "message" => "Employee number verified successfully!", "eid" => $data["eid"])));
  	} else {
  		die(json_encode(array("result" => "fail", "message" => "Employee number was not found!")));
  	}
  }

  public function attendance(){
  	header('Access-Control-Allow-Origin: *');
  	$data = $_GET;

  	if(!isset($data["eid"])){
  		die(json_encode(array("result" => "fail", "message" => "Some fields are not set!")));
  	}

  	if($this->panel->verifyEmployeeNumber($data["eid"]) > 0){
  		$attendance = $this->panel->getAttendance(array("eid" => $data["eid"], "month" => date("n")));
  		die(json_encode($attendance));
  	} else {
  		die(json_encode(array()));
  	}
  }
}