<?php
/* Smarty version 3.1.30, created on 2018-10-11 08:16:53
  from "C:\wamp64\www\marikina\templates\default\header.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5bbf06f5138fa1_74521269',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1a7a1d44c8ab334adef125244dbc9be1b5f83414' => 
    array (
      0 => 'C:\\wamp64\\www\\marikina\\templates\\default\\header.html',
      1 => 1539137353,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bbf06f5138fa1_74521269 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo @constant('site_url');?>
/templates/default/assets/images/favicon.ico">
    <title>SDO - MARIKINA CITY</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo @constant('site_url');?>
/templates/default/assets/css/lib/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo @constant('site_url');?>
/templates/default/assets/css/lib/chartist/chartist.min.css" rel="stylesheet">
    <link href="<?php echo @constant('site_url');?>
/templates/default/assets/css/lib/owl.carousel.min.css" rel="stylesheet" />
    <link href="<?php echo @constant('site_url');?>
/templates/default/assets/css/lib/owl.theme.default.min.css" rel="stylesheet" />
    <link href="<?php echo @constant('site_url');?>
/templates/default/assets/css/jquery-ui.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo @constant('site_url');?>
/templates/default/assets/css/izimodal.min.css" rel="stylesheet">
    <link href="<?php echo @constant('site_url');?>
/templates/default/assets/css/jquery.timepicker.min.css" rel="stylesheet">
    <link href="<?php echo @constant('site_url');?>
/templates/default/assets/css/lightbox.min.css" rel="stylesheet">
    <link href="<?php echo @constant('site_url');?>
/templates/default/assets/css/helper.css" rel="stylesheet">
    <link href="<?php echo @constant('site_url');?>
/templates/default/assets/css/style.css" rel="stylesheet">
    <link href="<?php echo @constant('site_url');?>
/templates/default/assets/css/custom.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:** -->
    <!--[if lt IE 9]>
    <?php echo '<script'; ?>
 src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"><?php echo '</script'; ?>
>
    <![endif]-->
    <?php echo '<script'; ?>
>
        var site_url = "<?php echo @constant('site_url');?>
";
        var date = new Date();
    <?php echo '</script'; ?>
>
</head>

<body class="<?php if ($_smarty_tpl->tpl_vars['page']->value == 'login') {?>login-bg <?php }?>fix-header fix-sidebar">
<!-- Preloader - style you can find in spinners.css -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div><?php }
}
