<?php
/* Smarty version 3.1.30, created on 2018-07-05 09:52:04
  from "C:\wamp\www\marikina\templates\default\pages\login.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b3dea4453d2e0_84271276',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'adc311250e9666b46433d89841ce0704add96446' => 
    array (
      0 => 'C:\\wamp\\www\\marikina\\templates\\default\\pages\\login.html',
      1 => 1528357851,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b3dea4453d2e0_84271276 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- Main wrapper  -->
<div id="main-wrapper">
    <div class="unix-login">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-4">
                    <div class="login-content card">
                        <div class="login-form">
                            <div class="login-logo">
                                <b class="logo-small"><img src="<?php echo @constant('site_url');?>
/templates/default/assets/images/ntek-logo.png" alt="homepage" class="dark-logo" /></b>
                                <span class="logo-big"><img src="<?php echo @constant('site_url');?>
/templates/default/assets/images/ntek-logo-text.png" alt="homepage" class="dark-logo" /></span>
                            </div>
                            <form id="login">
                                <div class="form-group">
                                    <label><i class="fa fa-user"></i> Username</label>
                                    <input type="text" name="username" class="form-control" placeholder="Username">
                                </div>
                                <div class="form-group">
                                    <label><i class="fa fa-asterisk"></i> Password</label>
                                    <input type="password" name="password" class="form-control" placeholder="Password">
                                </div>
                                <button type="submit" class="btn btn-primary btn-flat m-b-30 m-t-30"><i class="fa fa-check-circle"></i> Login</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Wrapper --><?php }
}
