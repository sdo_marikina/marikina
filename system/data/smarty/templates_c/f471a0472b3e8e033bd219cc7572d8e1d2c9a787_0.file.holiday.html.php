<?php
/* Smarty version 3.1.30, created on 2018-08-14 15:35:17
  from "C:\wamp\www\marikina\templates\default\pages\holiday.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b72863583f7a2_72633755',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f471a0472b3e8e033bd219cc7572d8e1d2c9a787' => 
    array (
      0 => 'C:\\wamp\\www\\marikina\\templates\\default\\pages\\holiday.html',
      1 => 1534232116,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../modules/navbar.html' => 1,
    'file:../modules/sidebar.html' => 1,
  ),
),false)) {
function content_5b72863583f7a2_72633755 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- Main wrapper  -->
<div id="main-wrapper">
    <?php $_smarty_tpl->_subTemplateRender("file:../modules/navbar.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <?php $_smarty_tpl->_subTemplateRender("file:../modules/sidebar.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <!-- Page wrapper  -->
    <div class="page-wrapper">
        <!-- Bread crumb -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3><i class="fa fa-calendar-o"></i> Edit Holiday</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Edit Holiday</li>
                </ol>
            </div>
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-12">
                    <div class="card card-outline-primary">
                        <div class="card-header">
                            <h4 class="m-b-0 text-white"><i class="fa fa-edit"></i> Edit Holiday Info</h4>
                        </div>
                        <div class="card-body">
                            <form id="edit-holiday-form" class="form-horizontal">
                                <div class="form-body">
                                    <hr class="m-t-0 m-b-40">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Name</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="name" class="form-control" placeholder="Andrew Mark" value="<?php echo ucwords($_smarty_tpl->tpl_vars['holiday']->value['name']);?>
">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group has-danger row">
                                                <label class="control-label text-right col-md-3">Date</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="holiday" id="holiday-date" class="form-control" placeholder="06-26" value="<?php echo $_smarty_tpl->tpl_vars['holiday']->value['date'];?>
">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['holiday']->value['id'];?>
">
                                </div>
                                <hr>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i> Save Changes</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6"> </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Page Content -->
        </div>
        <!-- End Container fluid  -->
        <!-- footer -->
        <footer class="footer text-center">All Rights Reserved &copy; <?php echo date("Y");?>
</footer>
        <!-- End footer -->
    </div>
    <!-- End Page wrapper  -->
</div>
<!-- End Wrapper --><?php }
}
