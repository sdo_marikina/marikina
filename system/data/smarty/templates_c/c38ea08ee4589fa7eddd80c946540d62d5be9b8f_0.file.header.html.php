<?php
/* Smarty version 3.1.30, created on 2018-06-18 09:50:54
  from "C:\xampp\htdocs\marikina\templates\default\header.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b270ffe435a73_78119770',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c38ea08ee4589fa7eddd80c946540d62d5be9b8f' => 
    array (
      0 => 'C:\\xampp\\htdocs\\marikina\\templates\\default\\header.html',
      1 => 1528703522,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b270ffe435a73_78119770 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo @constant('site_url');?>
/templates/default/assets/images/favicon.ico">
    <title>NTEK Attendance Management System</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo @constant('site_url');?>
/templates/default/assets/css/lib/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo @constant('site_url');?>
/templates/default/assets/css/lib/chartist/chartist.min.css" rel="stylesheet">
    <link href="<?php echo @constant('site_url');?>
/templates/default/assets/css/lib/owl.carousel.min.css" rel="stylesheet" />
    <link href="<?php echo @constant('site_url');?>
/templates/default/assets/css/lib/owl.theme.default.min.css" rel="stylesheet" />
    <!-- Custom CSS -->
    <link href="<?php echo @constant('site_url');?>
/templates/default/assets/css/helper.css" rel="stylesheet">
    <link href="<?php echo @constant('site_url');?>
/templates/default/assets/css/style.css" rel="stylesheet">
    <link href="<?php echo @constant('site_url');?>
/templates/default/assets/css/izimodal.min.css" rel="stylesheet">
    <link href="<?php echo @constant('site_url');?>
/templates/default/assets/css/custom.css" rel="stylesheet">
    <link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:** -->
    <!--[if lt IE 9]>
    <?php echo '<script'; ?>
 src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"><?php echo '</script'; ?>
>
    <![endif]-->
    <?php echo '<script'; ?>
>
        var site_url = "<?php echo @constant('site_url');?>
";
        var date = new Date();
    <?php echo '</script'; ?>
>
</head>

<body class="<?php if ($_smarty_tpl->tpl_vars['page']->value == 'login') {?>login-bg <?php }?>fix-header fix-sidebar">
<!-- Preloader - style you can find in spinners.css -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div><?php }
}
