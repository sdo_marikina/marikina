<?php
/* Smarty version 3.1.30, created on 2018-09-14 15:42:50
  from "C:\wamp\www\marikina\templates\default\footer.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b9b667a6dc813_08835046',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '85a24a6838e3391cce40b7a2c8628c725c2aef9a' => 
    array (
      0 => 'C:\\wamp\\www\\marikina\\templates\\default\\footer.html',
      1 => 1536903951,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./modules/widgets.html' => 1,
  ),
),false)) {
function content_5b9b667a6dc813_08835046 (Smarty_Internal_Template $_smarty_tpl) {
if (isset($_SESSION['logged']) && $_smarty_tpl->tpl_vars['page']->value != "panel") {
$_smarty_tpl->_subTemplateRender("file:./modules/widgets.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }?>

<!-- All Jquery -->
<?php echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/lib/jquery/jquery.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/lib/jquery-ui/jquery-ui.min.js"><?php echo '</script'; ?>
>

<!-- Bootstrap tether Core JavaScript -->
<?php echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/lib/bootstrap/js/popper.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/lib/bootstrap/js/bootstrap.min.js"><?php echo '</script'; ?>
>

<!-- slimscrollbar scrollbar JavaScript -->
<?php echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/jquery.slimscroll.js"><?php echo '</script'; ?>
>

<!--Menu sidebar -->
<?php echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/sidebarmenu.js"><?php echo '</script'; ?>
>

<!--stickey kit -->
<?php echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"><?php echo '</script'; ?>
>

<!-- Datatables -->
<?php if (isset($_SESSION['logged']) && $_smarty_tpl->tpl_vars['page']->value != "panel") {
echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/datatables/datatables.min.js"><?php echo '</script'; ?>
>
<?php if ($_smarty_tpl->tpl_vars['page']->value != "history" || $_smarty_tpl->tpl_vars['page']->value != "panel") {
echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/datatables/dataTables.liveAjax.js"><?php echo '</script'; ?>
>
<?php }
}
if ($_smarty_tpl->tpl_vars['page']->value == "home") {
echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/datatables/init-dashboard.js"><?php echo '</script'; ?>
>
<?php }
if ($_smarty_tpl->tpl_vars['page']->value == "history") {
echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/datatables/init-history.js"><?php echo '</script'; ?>
>
<?php }
if ($_smarty_tpl->tpl_vars['page']->value == "missed") {
echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/datatables/init-missed.js"><?php echo '</script'; ?>
>
<?php }
if ($_smarty_tpl->tpl_vars['page']->value == "employees") {
echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/datatables/init-employees.js"><?php echo '</script'; ?>
>
<?php }
if ($_smarty_tpl->tpl_vars['page']->value == "positions") {
echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/datatables/init-positions.js"><?php echo '</script'; ?>
>
<?php }
if ($_smarty_tpl->tpl_vars['page']->value == "departments") {
echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/datatables/init-departments.js"><?php echo '</script'; ?>
>
<?php }
if ($_smarty_tpl->tpl_vars['page']->value == "holidays") {
echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/datatables/init-holidays.js"><?php echo '</script'; ?>
>
<?php }
if ($_smarty_tpl->tpl_vars['page']->value == "accounts") {
echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/datatables/init-accounts.js"><?php echo '</script'; ?>
>
<?php }
if ($_smarty_tpl->tpl_vars['page']->value == "monitors") {
echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/datatables/init-monitors.js"><?php echo '</script'; ?>
>
<?php }
if ($_smarty_tpl->tpl_vars['page']->value == "devices") {
echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/datatables/init-devices.js"><?php echo '</script'; ?>
>
<?php }?>

<!--Custom JavaScript -->
<?php echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/izimodal.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/sweetalert.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/lightbox.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/jquery.timepicker.min.js"><?php echo '</script'; ?>
>
<?php if ($_smarty_tpl->tpl_vars['page']->value == "panel") {
echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/jsonToTable.min.js"><?php echo '</script'; ?>
>
<?php }
if (isset($_SESSION['logged']) && $_smarty_tpl->tpl_vars['page']->value != "panel") {
echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/admin.js"><?php echo '</script'; ?>
>
<?php }
if ($_smarty_tpl->tpl_vars['page']->value == "login" || $_smarty_tpl->tpl_vars['page']->value == "panel") {
echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/custom.js"><?php echo '</script'; ?>
>
<?php }
echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/scripts.js"><?php echo '</script'; ?>
>

</body>
</html><?php }
}
