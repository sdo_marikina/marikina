<?php
/* Smarty version 3.1.30, created on 2018-06-27 16:20:50
  from "C:\xampp\htdocs\marikina\templates\default\modules\sidebar.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b3348e22358e8_27058125',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8ebac4785c99d26b833e1e60facd16a9cfc375bb' => 
    array (
      0 => 'C:\\xampp\\htdocs\\marikina\\templates\\default\\modules\\sidebar.html',
      1 => 1530087648,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b3348e22358e8_27058125 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- Left Sidebar  -->
<div class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-label">Dashboard</li>
                <li><a href="<?php echo @constant('site_url');?>
" aria-expanded="false"><i class="fa fa-list"></i><span class="hide-menu">Attendance Logs</span></a></li>
                <li class="nav-label">Settings</li>
                <li><a href="<?php echo @constant('site_url');?>
/admin/employees" aria-expanded="false"><i class="fa fa-users"></i><span class="hide-menu">Employees</span></a></li>
                <li><a href="<?php echo @constant('site_url');?>
/admin/generate" aria-expanded="false"><i class="fa fa-calendar"></i><span class="hide-menu">Schedules</span></a></li>
                <li><a href="<?php echo @constant('site_url');?>
/admin/generate" aria-expanded="false"><i class="fa fa-calendar-o"></i><span class="hide-menu">Holidays</span></a></li>
                <li><a href="<?php echo @constant('site_url');?>
/admin/accounts" aria-expanded="false"><i class="fa fa-cogs"></i><span class="hide-menu">System Admins</span></a></li>
                <li class="nav-label">System</li>
                <li><a href="<?php echo @constant('site_url');?>
/admin/monitors" aria-expanded="false"><i class="fa fa-desktop"></i><span class="hide-menu">Manage Monitors</span></a></li>
                <li><a href="<?php echo @constant('site_url');?>
/admin/devices" aria-expanded="false"><i class="fa fa-tablet"></i><span class="hide-menu">Manage Devices</span></a></li>
                <li><a href="javascript:void(0)" data-iziModal-open="#configurations"><i class="fa fa-cogs"></i><span class="hide-menu">Configurations</span></a></li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</div>
<!-- End Left Sidebar  --><?php }
}
