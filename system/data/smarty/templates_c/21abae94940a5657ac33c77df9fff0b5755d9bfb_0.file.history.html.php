<?php
/* Smarty version 3.1.30, created on 2018-10-10 03:32:21
  from "C:\wamp64\www\sdo_marikina_systems\marikina\templates\default\pages\history.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5bbd72c5a07df0_14148977',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '21abae94940a5657ac33c77df9fff0b5755d9bfb' => 
    array (
      0 => 'C:\\wamp64\\www\\sdo_marikina_systems\\marikina\\templates\\default\\pages\\history.html',
      1 => 1539137353,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../modules/navbar.html' => 1,
    'file:../modules/sidebar.html' => 1,
  ),
),false)) {
function content_5bbd72c5a07df0_14148977 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- Main wrapper  -->
<div id="main-wrapper">
    <?php $_smarty_tpl->_subTemplateRender("file:../modules/navbar.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <?php $_smarty_tpl->_subTemplateRender("file:../modules/sidebar.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <!-- Page wrapper  -->
    <div class="page-wrapper">
        <!-- Bread crumb -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-mute"><i class="fa fa-users"></i> Attendance History</h3>
            </div>
            <div class="col-md-7 text-right">
                <button class="btn btn-warning btn-sm" data-iziModal-open="#report"><i class="fa fa-cloud-upload"></i> Submit DTR Report</button>
                <button class="btn btn-primary btn-sm" data-iziModal-open="#form7"><i class="fa fa-check-circle-o"></i> Submit Form7</button>
            </div>
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <div class="pull-left m-r-15 m-b-15">
                                    <input type="text" id="date-min" class="form-control" placeholder="Select Start Date">
                                    <input type="text" id="date-max" class="form-control" placeholder="Select End Date">
                                </div>
                                <table id="history" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%"></table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Page Content -->
        </div>
        <!-- End Container fluid  -->
        <!-- footer -->
        <footer class="footer text-center">All Rights Reserved &copy; <?php echo date("Y");?>
</footer>
        <!-- End footer -->
    </div>
    <!-- End Page wrapper  -->
</div>
<!-- End Wrapper --><?php }
}
