<?php
/* Smarty version 3.1.30, created on 2018-06-27 13:41:16
  from "C:\xampp\htdocs\marikina\templates\default\modules\widgets.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b33237c525288_94517766',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '28611de0e603171196f672efb18ea8fcf2269f0a' => 
    array (
      0 => 'C:\\xampp\\htdocs\\marikina\\templates\\default\\modules\\widgets.html',
      1 => 1530078073,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b33237c525288_94517766 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="generate" class="iziModal" data-iziModal-title="Generate DTR Report"  data-iziModal-subtitle="Daily time record with computation generation"  data-iziModal-icon="fa fa-cloud-upload">
    <form>
	  <div class="form-group">
	    <label for="generate-min">Select Start Date:</label>
	    <input type="text" id="generate-min" class="form-control" placeholder="<?php echo date('m/d/Y');?>
">
	  </div>
	  <div class="form-group">
	    <label for="generate-max">Select End Date:</label>
	    <input type="text" id="generate-max" class="form-control" placeholder="<?php echo date('m/d/Y');?>
">
	  </div>
	  <div class="form-group">
	    <label for="department">Department</label>
	    <select class="form-control">
	    	<option selected>All</option>
	    	<option>Registrar</option>
	    	<option>Office</option>
	    </select>
	  </div>
	  <div class="form-group">
	    <label for="department">Employment Type</label>
	    <select class="form-control">
	    	<option selected>All</option>
	    	<option>Teaching</option>
	    	<option>Non-Teaching</option>
	    </select>
	  </div>
	  <button type="submit" class="btn btn-success"><i class="fa fa-check-circle"></i> Submit</button>
	</form>
</div>

<div id="configurations" class="iziModal" data-iziModal-title="System Configurations"  data-iziModal-subtitle="Adjust system core settings and variables"  data-iziModal-icon="fa fa-cogs">
    <form>
	  <div class="form-group">
	    <label for="generate-min">Select Start Date:</label>
	    <input type="text" id="generate-min" class="form-control" placeholder="<?php echo date('m/d/Y');?>
">
	  </div>
	  <div class="form-group">
	    <label for="generate-max">Select End Date:</label>
	    <input type="text" id="generate-max" class="form-control" placeholder="<?php echo date('m/d/Y');?>
">
	  </div>
	  <div class="form-group">
	    <label for="department">Department</label>
	    <select class="form-control">
	    	<option selected>All</option>
	    	<option>Registrar</option>
	    	<option>Office</option>
	    </select>
	  </div>
	  <div class="form-group">
	    <label for="department">Employment Type</label>
	    <select class="form-control">
	    	<option selected>All</option>
	    	<option>Teaching</option>
	    	<option>Non-Teaching</option>
	    </select>
	  </div>
	  <button type="submit" class="btn btn-success"><i class="fa fa-check-circle"></i> Submit</button>
	</form>
</div><?php }
}
