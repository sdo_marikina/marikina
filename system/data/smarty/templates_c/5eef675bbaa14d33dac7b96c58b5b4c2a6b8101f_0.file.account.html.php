<?php
/* Smarty version 3.1.30, created on 2018-10-15 03:51:52
  from "C:\wamp64\www\marikina\templates\default\pages\account.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5bc40ed8329948_57368373',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5eef675bbaa14d33dac7b96c58b5b4c2a6b8101f' => 
    array (
      0 => 'C:\\wamp64\\www\\marikina\\templates\\default\\pages\\account.html',
      1 => 1539137353,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../modules/navbar.html' => 1,
    'file:../modules/sidebar.html' => 1,
  ),
),false)) {
function content_5bc40ed8329948_57368373 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- Main wrapper  -->
<div id="main-wrapper">
    <?php $_smarty_tpl->_subTemplateRender("file:../modules/navbar.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <?php $_smarty_tpl->_subTemplateRender("file:../modules/sidebar.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <!-- Page wrapper  -->
    <div class="page-wrapper">
        <!-- Bread crumb -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3><i class="fa fa-cog"></i> Edit Administrator</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Edit Administrator</li>
                </ol>
            </div>
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-12">
                    <div class="card card-outline-primary">
                        <div class="card-header">
                            <h4 class="m-b-0 text-white"><i class="fa fa-edit"></i> Edit Admin Account</h4>
                        </div>
                        <div class="card-body">
                            <form id="edit-account-form" class="form-horizontal">
                                <div class="form-body">
                                    <hr class="m-t-0 m-b-40">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Username</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="username" class="form-control" placeholder="Andrew Mark" value="<?php echo $_smarty_tpl->tpl_vars['account']->value['username'];?>
">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group has-danger row">
                                                <label class="control-label text-right col-md-3">Password</label>
                                                <div class="col-md-9">
                                                    <input type="password" name="password" class="form-control" placeholder="Enter new password, leave empty to keep the old one">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['account']->value['id'];?>
">
                                </div>
                                <hr>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i> Save Changes</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6"> </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Page Content -->
        </div>
        <!-- End Container fluid  -->
        <!-- footer -->
        <footer class="footer text-center">All Rights Reserved &copy; <?php echo date("Y");?>
</footer>
        <!-- End footer -->
    </div>
    <!-- End Page wrapper  -->
</div>
<!-- End Wrapper --><?php }
}
