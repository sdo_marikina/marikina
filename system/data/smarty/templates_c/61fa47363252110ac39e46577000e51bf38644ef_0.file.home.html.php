<?php
/* Smarty version 3.1.30, created on 2018-06-27 16:20:28
  from "C:\xampp\htdocs\marikina\templates\default\pages\home.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b3348cc57c709_14180058',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '61fa47363252110ac39e46577000e51bf38644ef' => 
    array (
      0 => 'C:\\xampp\\htdocs\\marikina\\templates\\default\\pages\\home.html',
      1 => 1530087626,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../modules/navbar.html' => 1,
    'file:../modules/sidebar.html' => 1,
  ),
),false)) {
function content_5b3348cc57c709_14180058 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- Main wrapper  -->
<div id="main-wrapper">
    <?php $_smarty_tpl->_subTemplateRender("file:../modules/navbar.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <?php $_smarty_tpl->_subTemplateRender("file:../modules/sidebar.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <!-- Page wrapper  -->
    <div class="page-wrapper">
        <!-- Bread crumb -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-mute"><i class="fa fa-users"></i> Attendance Monitor</h3>
            </div>
            <div class="col-md-7 text-right">
                <button class="btn btn-warning btn-sm" data-iziModal-open="#generate"><i class="fa fa-cloud-upload"></i> Generate DTR Report</button>
            </div>
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <div class="pull-left m-r-15 m-b-15">
                                    <label for="date-min">From:</label>
                                    <input type="text" id="date-min" class="form-control" placeholder="Select Start Date" />
                                    <label for="date-max">To:</label>
                                    <input type="text" id="date-max" class="form-control" placeholder="Select End Date" />
                                </div>
                                <table id="access-logs" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%"></table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Page Content -->
        </div>
        <!-- End Container fluid  -->
        <!-- footer -->
        <footer class="footer text-center">All Rights Reserved &copy; <?php echo date("Y");?>
</footer>
        <!-- End footer -->
    </div>
    <!-- End Page wrapper  -->
</div>
<!-- End Wrapper --><?php }
}
