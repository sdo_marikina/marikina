<?php
/* Smarty version 3.1.30, created on 2018-08-14 14:29:31
  from "C:\wamp\www\marikina\templates\default\pages\department.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b7276cbb8cfb0_76567705',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4a0a5536544cfc66c31c97a5cbbfe1f13e5fe8aa' => 
    array (
      0 => 'C:\\wamp\\www\\marikina\\templates\\default\\pages\\department.html',
      1 => 1534227899,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../modules/navbar.html' => 1,
    'file:../modules/sidebar.html' => 1,
  ),
),false)) {
function content_5b7276cbb8cfb0_76567705 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- Main wrapper  -->
<div id="main-wrapper">
    <?php $_smarty_tpl->_subTemplateRender("file:../modules/navbar.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <?php $_smarty_tpl->_subTemplateRender("file:../modules/sidebar.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <!-- Page wrapper  -->
    <div class="page-wrapper">
        <!-- Bread crumb -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3><i class="fa fa-users"></i> Edit Department</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Edit Department</li>
                </ol>
            </div>
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-12">
                    <div class="card card-outline-primary">
                        <div class="card-header">
                            <h4 class="m-b-0 text-white"><i class="fa fa-edit"></i> Edit Department Info</h4>
                        </div>
                        <div class="card-body">
                            <form id="edit-department-form" class="form-horizontal">
                                <div class="form-body">
                                    <hr class="m-t-0 m-b-40">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Name</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="name" class="form-control" placeholder="Andrew Mark" value="<?php echo ucwords($_smarty_tpl->tpl_vars['department']->value['name']);?>
">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group has-danger row">
                                                <label class="control-label text-right col-md-3">Description</label>
                                                <div class="col-md-9">
                                                    <textarea name="description" class="form-control" rows="3"><?php echo $_smarty_tpl->tpl_vars['department']->value['description'];?>
</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['department']->value['id'];?>
">
                                </div>
                                <hr>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i> Save Changes</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6"> </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Page Content -->
        </div>
        <!-- End Container fluid  -->
        <!-- footer -->
        <footer class="footer text-center">All Rights Reserved &copy; <?php echo date("Y");?>
</footer>
        <!-- End footer -->
    </div>
    <!-- End Page wrapper  -->
</div>
<!-- End Wrapper --><?php }
}
