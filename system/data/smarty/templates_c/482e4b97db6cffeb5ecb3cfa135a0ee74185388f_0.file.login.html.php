<?php
/* Smarty version 3.1.30, created on 2018-10-11 08:16:53
  from "C:\wamp64\www\marikina\templates\default\pages\login.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5bbf06f515fb07_64538784',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '482e4b97db6cffeb5ecb3cfa135a0ee74185388f' => 
    array (
      0 => 'C:\\wamp64\\www\\marikina\\templates\\default\\pages\\login.html',
      1 => 1539137353,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bbf06f515fb07_64538784 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- Main wrapper  -->
<div id="main-wrapper">
    <div class="unix-login">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-4">
                    <div class="login-content card">
                        <div class="login-form">
                            <div class="login-logo">
                               <img src="<?php echo @constant('site_url');?>
/templates/default/assets/images/ntek-logo.png" style="width: 100px; height: 100px; text-shadow: 0px 20px 50px rgba(0,0,0,0.5); margin-top: -130px;" />
                                <p>Daily Time Record | <strong>SDO MARIKINA</strong></p>
                            </div>
                            <form id="login">
                                <div class="form-group">
                                    <label><i class="fa fa-user"></i> Username</label>
                                    <input type="text" name="username" class="form-control" placeholder="Username">
                                </div>
                                <div class="form-group">
                                    <label><i class="fa fa-asterisk"></i> Password</label>
                                    <input type="password" name="password" class="form-control" placeholder="Password">
                                </div>
                                <button type="submit" class="btn btn-primary btn-flat m-b-30 m-t-30"><i class="fa fa-check-circle"></i> Login</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Wrapper --><?php }
}
