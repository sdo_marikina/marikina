<?php
/* Smarty version 3.1.30, created on 2018-08-14 10:35:17
  from "C:\wamp\www\marikina\templates\default\pages\panel.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b723fe583ea85_46308547',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'eafc902d853cde56ce49ed0371a574931a8be9fa' => 
    array (
      0 => 'C:\\wamp\\www\\marikina\\templates\\default\\pages\\panel.html',
      1 => 1534213363,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b723fe583ea85_46308547 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- Main wrapper  -->

<div class="error-page" id="wrapper">
    <div class="error-box">
        <div class="error-body text-center">
            <h3 class="text-uppercase"><i class="fa fa-user"></i> Employee Dashboard</h3>
            <p class="text-muted m-t-30 m-b-30">Please enter your employee number to view your recorded attendance history for this month</p>
            <div class="container">
            	<form id="panel-form">
	           		<div class="input-group col-5 mb-3" style="margin: 0 auto">
					  <input type="text" name="eid" class="form-control input-lg" placeholder="12BS86-7516SC8">
					  <div class="input-group-append">
					    <button type="submit" class="btn btn-primary" type="button"><i class="fa fa-check-circle-o"></i> Submit</button>
					  </div>
					</div>
				</form>

				<div class="table"></div>
            </div>	
        </div>
    </div>
</div>
<!-- End Wrapper --><?php }
}
