<?php
/* Smarty version 3.1.30, created on 2018-10-10 03:02:38
  from "C:\wamp64\www\sdo_marikina_systems\marikina\templates\default\pages\home.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5bbd6bce7ad615_92052806',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ea5409011247c730e9a8134a33fb094c4fb0a017' => 
    array (
      0 => 'C:\\wamp64\\www\\sdo_marikina_systems\\marikina\\templates\\default\\pages\\home.html',
      1 => 1539137353,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../modules/navbar.html' => 1,
    'file:../modules/sidebar.html' => 1,
  ),
),false)) {
function content_5bbd6bce7ad615_92052806 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- Main wrapper  -->
<div id="main-wrapper">
    <?php $_smarty_tpl->_subTemplateRender("file:../modules/navbar.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <?php $_smarty_tpl->_subTemplateRender("file:../modules/sidebar.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <!-- Page wrapper  -->
    <div class="page-wrapper">
        <!-- Bread crumb -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-mute"><i class="fa fa-line-chart"></i> Live Attendance Monitor</h3>
            </div>
            <div class="col-md-7 text-right">
                <button class="btn btn-warning btn-sm" data-iziModal-open="#report"><i class="fa fa-cloud-upload"></i> Submit DTR Report</button>
                <button class="btn btn-primary btn-sm" data-iziModal-open="#form7"><i class="fa fa-check-circle-o"></i> Submit Form7</button>
            </div>
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="access-logs" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%"></table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Page Content -->
        </div>
        <!-- End Container fluid  -->
        <!-- footer -->
        <footer class="footer text-center">All Rights Reserved &copy; <?php echo date("Y");?>
</footer>
        <!-- End footer -->
    </div>
    <!-- End Page wrapper  -->
</div>
<!-- End Wrapper --><?php }
}
