<?php
/* Smarty version 3.1.30, created on 2018-10-11 10:16:54
  from "C:\wamp64\www\marikina\templates\default\modules\sidebar.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5bbf23164ed271_33094872',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '03632ef0eea9b198f14048090d94879b80edc2f8' => 
    array (
      0 => 'C:\\wamp64\\www\\marikina\\templates\\default\\modules\\sidebar.html',
      1 => 1539137353,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bbf23164ed271_33094872 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- Left Sidebar  -->
<div class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-label">Dashboard</li>
                <li><a href="<?php echo @constant('site_url');?>
" aria-expanded="false"><i class="fa fa-line-chart"></i><span class="hide-menu">Live Attendance</span></a></li>
                <li><a href="<?php echo @constant('site_url');?>
/admin/history" aria-expanded="false"><i class="fa fa-history"></i><span class="hide-menu">Attendance History</span></a></li>
                <li class="nav-label">Settings</li>
                <li><a href="<?php echo @constant('site_url');?>
/admin/employees" aria-expanded="false"><i class="fa fa-users"></i><span class="hide-menu">Employees</span></a></li>
                <li><a href="<?php echo @constant('site_url');?>
/admin/positions" aria-expanded="false"><i class="fa fa-vcard"></i><span class="hide-menu">Positions</span></a></li>
                <li><a href="<?php echo @constant('site_url');?>
/admin/departments" aria-expanded="false"><i class="fa fa-tasks"></i><span class="hide-menu">Departments</span></a></li>
                <li><a href="<?php echo @constant('site_url');?>
/admin/holidays" aria-expanded="false"><i class="fa fa-calendar-o"></i><span class="hide-menu">Holidays</span></a></li>
                <li><a href="<?php echo @constant('site_url');?>
/admin/accounts" aria-expanded="false"><i class="fa fa-cogs"></i><span class="hide-menu">System Admins</span></a></li>
                <li class="nav-label">System</li>
                <li><a href="javascript:void(0)" data-iziModal-open="#configurations"><i class="fa fa-cogs"></i><span class="hide-menu">Configurations</span></a></li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</div>
<!-- End Left Sidebar  --><?php }
}
