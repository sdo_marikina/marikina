<?php
/* Smarty version 3.1.30, created on 2018-09-14 15:59:51
  from "C:\wamp\www\marikina\templates\default\pages\employee.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b9b6a7795e607_02398476',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7c0631996776947260828f69bcfe40af090ce2bb' => 
    array (
      0 => 'C:\\wamp\\www\\marikina\\templates\\default\\pages\\employee.html',
      1 => 1536911916,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../modules/navbar.html' => 1,
    'file:../modules/sidebar.html' => 1,
  ),
),false)) {
function content_5b9b6a7795e607_02398476 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_html_options')) require_once 'C:\\wamp\\www\\marikina\\system\\libraries\\smarty\\plugins\\function.html_options.php';
?>
<!-- Main wrapper  -->
<div id="main-wrapper">
    <?php $_smarty_tpl->_subTemplateRender("file:../modules/navbar.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <?php $_smarty_tpl->_subTemplateRender("file:../modules/sidebar.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <!-- Page wrapper  -->
    <div class="page-wrapper">
        <!-- Bread crumb -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3><i class="fa fa-users"></i> Edit Employee</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Edit Employee</li>
                </ol>
            </div>
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-12">
                    <div class="card card-outline-primary">
                        <div class="card-header">
                            <h4 class="m-b-0 text-white"><i class="fa fa-edit"></i> Edit Employee Account</h4>
                        </div>
                        <div class="card-body">
                            <form id="edit-employee-form" class="form-horizontal">
                                <div class="form-body">
                                    <hr class="m-t-0 m-b-40">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">First Name</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="fname" class="form-control" placeholder="Andrew Mark" value="<?php echo ucwords($_smarty_tpl->tpl_vars['employee']->value['fname']);?>
">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group has-danger row">
                                                <label class="control-label text-right col-md-3">Employment Type</label>
                                                <div class="col-md-9">
                                                    <?php echo smarty_function_html_options(array('name'=>"type",'class'=>"form-control",'options'=>$_smarty_tpl->tpl_vars['types']->value,'selected'=>$_smarty_tpl->tpl_vars['employee']->value['type']),$_smarty_tpl);?>

                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Middle Name</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="mname" class="form-control" placeholder="Pwerta" value="<?php echo ucwords($_smarty_tpl->tpl_vars['employee']->value['mname']);?>
">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group has-danger row">
                                                <label class="control-label text-right col-md-3">Department</label>
                                                <div class="col-md-9">
                                                    <?php echo smarty_function_html_options(array('name'=>"department",'class'=>"form-control",'options'=>$_smarty_tpl->tpl_vars['departments']->value,'selected'=>$_smarty_tpl->tpl_vars['employee']->value['department']),$_smarty_tpl);?>

                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Last Name</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="lname" class="form-control" placeholder="Sentido" value="<?php echo ucwords($_smarty_tpl->tpl_vars['employee']->value['lname']);?>
">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group has-danger row">
                                                <label class="control-label text-right col-md-3">Position</label>
                                                <div class="col-md-9">
                                                    <?php echo smarty_function_html_options(array('name'=>"position",'class'=>"form-control",'options'=>$_smarty_tpl->tpl_vars['positions']->value,'selected'=>$_smarty_tpl->tpl_vars['employee']->value['position']),$_smarty_tpl);?>

                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Employee Number</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="eid" class="form-control" placeholder="Enter new employee number or leave empty to keep the old one">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group has-danger row">
                                                <label class="control-label text-right col-md-3">Basic Pay</label>
                                                <div class="col-md-9">
                                                    <input type="number" name="basic" class="form-control" placeholder="15000" value="<?php echo $_smarty_tpl->tpl_vars['employee']->value['basic_pay'];?>
">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Time-In</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="tin" class="timepicker form-control" value="<?php echo $_smarty_tpl->tpl_vars['schedule']->value[0];?>
">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Time-Out</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="tout" class="timepicker form-control" value="<?php echo $_smarty_tpl->tpl_vars['schedule']->value[1];?>
">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Break-In</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="bin" class="timepicker form-control" value="<?php echo $_smarty_tpl->tpl_vars['schedule']->value[2];?>
">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Break-Out</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="bout" class="timepicker form-control" value="<?php echo $_smarty_tpl->tpl_vars['schedule']->value[3];?>
">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['employee']->value['id'];?>
">
                                </div>
                                <hr>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i> Save Changes</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6"> </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Page Content -->
        </div>
        <!-- End Container fluid  -->
        <!-- footer -->
        <footer class="footer text-center">All Rights Reserved &copy; <?php echo date("Y");?>
</footer>
        <!-- End footer -->
    </div>
    <!-- End Page wrapper  -->
</div>
<!-- End Wrapper --><?php }
}
