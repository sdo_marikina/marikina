<?php
/* Smarty version 3.1.30, created on 2018-10-10 03:02:38
  from "C:\wamp64\www\sdo_marikina_systems\marikina\templates\default\modules\navbar.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5bbd6bce855082_73509177',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7af866895b8a8b9612a61566a8bdfa6a78dc8928' => 
    array (
      0 => 'C:\\wamp64\\www\\sdo_marikina_systems\\marikina\\templates\\default\\modules\\navbar.html',
      1 => 1539137353,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bbd6bce855082_73509177 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- header header  -->
<div class="header">
    <nav class="navbar top-navbar navbar-expand-md navbar-light">
        <!-- Logo -->
        <div class="navbar-header">
            <a class="navbar-brand" href="<?php echo @constant('site_url');?>
">
                <!-- Logo icon -->
                <b class="logo-small"><img src="<?php echo @constant('site_url');?>
/templates/default/assets/images/ntek-logo.png" alt="homepage" class="dark-logo" /></b>
                <!--End Logo icon -->
                <!-- Logo text -->
                <span class="logo-big"><img src="<?php echo @constant('site_url');?>
/templates/default/assets/images/ntek-logo-text.png" alt="homepage" class="dark-logo" /></span>
            </a>
        </div>
        <!-- End Logo -->
        <div class="navbar-collapse">
            <!-- toggle and nav items -->
            <ul class="navbar-nav mr-auto mt-md-0">
                <!-- This is  -->
                <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a></li>
                <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted" href="javascript:void(0)"><i class="ti-menu"></i></a></li>
            </ul>
            <!-- User profile and search -->
            <ul class="navbar-nav my-lg-0">
                <!-- Profile -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i> <?php echo ucfirst($_smarty_tpl->tpl_vars['username']->value);?>
</a>
                    <div class="dropdown-menu dropdown-menu-right animated zoomIn">
                        <ul class="dropdown-user">
                            <li><a href="<?php echo @constant('site_url');?>
/admin/account/<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
"><i class="ti-settings"></i> Account Settings</a></li>
                            <li><a href="<?php echo @constant('site_url');?>
/admin/logout"><i class="fa fa-power-off"></i> Logout</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</div>
<!-- End header header --><?php }
}
