<?php
/* Smarty version 3.1.30, created on 2018-06-27 13:31:57
  from "C:\xampp\htdocs\marikina\templates\default\footer.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b33214dced908_02558883',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e3d8309a76c7c0f47dd4d189f97afe5f426b18a8' => 
    array (
      0 => 'C:\\xampp\\htdocs\\marikina\\templates\\default\\footer.html',
      1 => 1530077516,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./modules/widgets.html' => 1,
  ),
),false)) {
function content_5b33214dced908_02558883 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:./modules/widgets.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<!-- All Jquery -->
<?php echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/lib/jquery/jquery.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"><?php echo '</script'; ?>
>

<!-- Bootstrap tether Core JavaScript -->
<?php echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/lib/bootstrap/js/popper.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/lib/bootstrap/js/bootstrap.min.js"><?php echo '</script'; ?>
>

<!-- slimscrollbar scrollbar JavaScript -->
<?php echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/jquery.slimscroll.js"><?php echo '</script'; ?>
>

<!--Menu sidebar -->
<?php echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/sidebarmenu.js"><?php echo '</script'; ?>
>

<!--stickey kit -->
<?php echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"><?php echo '</script'; ?>
>

<!-- Datatables -->
<?php echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/datatables/datatables.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/datatables/dataTables.liveAjax.js"><?php echo '</script'; ?>
>
<?php if ($_smarty_tpl->tpl_vars['page']->value == "home") {
echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/datatables/init-dashboard.js"><?php echo '</script'; ?>
>
<?php }
if ($_smarty_tpl->tpl_vars['page']->value == "employees") {
echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/datatables/init-employees.js"><?php echo '</script'; ?>
>
<?php }
if ($_smarty_tpl->tpl_vars['page']->value == "accounts") {
echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/datatables/init-accounts.js"><?php echo '</script'; ?>
>
<?php }?>

<!--Custom JavaScript -->
<?php echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/izimodal.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/sweetalert.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/custom.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo @constant('site_url');?>
/templates/default/assets/js/scripts.js"><?php echo '</script'; ?>
>

</body>
</html><?php }
}
