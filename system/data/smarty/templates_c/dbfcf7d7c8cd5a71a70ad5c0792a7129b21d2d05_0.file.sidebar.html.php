<?php
/* Smarty version 3.1.30, created on 2018-10-10 03:02:38
  from "C:\wamp64\www\sdo_marikina_systems\marikina\templates\default\modules\sidebar.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5bbd6bce900064_95547949',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'dbfcf7d7c8cd5a71a70ad5c0792a7129b21d2d05' => 
    array (
      0 => 'C:\\wamp64\\www\\sdo_marikina_systems\\marikina\\templates\\default\\modules\\sidebar.html',
      1 => 1539137353,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bbd6bce900064_95547949 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- Left Sidebar  -->
<div class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-label">Dashboard</li>
                <li><a href="<?php echo @constant('site_url');?>
" aria-expanded="false"><i class="fa fa-line-chart"></i><span class="hide-menu">Live Attendance</span></a></li>
                <li><a href="<?php echo @constant('site_url');?>
/admin/history" aria-expanded="false"><i class="fa fa-history"></i><span class="hide-menu">Attendance History</span></a></li>
                <li class="nav-label">Settings</li>
                <li><a href="<?php echo @constant('site_url');?>
/admin/employees" aria-expanded="false"><i class="fa fa-users"></i><span class="hide-menu">Employees</span></a></li>
                <li><a href="<?php echo @constant('site_url');?>
/admin/positions" aria-expanded="false"><i class="fa fa-vcard"></i><span class="hide-menu">Positions</span></a></li>
                <li><a href="<?php echo @constant('site_url');?>
/admin/departments" aria-expanded="false"><i class="fa fa-tasks"></i><span class="hide-menu">Departments</span></a></li>
                <li><a href="<?php echo @constant('site_url');?>
/admin/holidays" aria-expanded="false"><i class="fa fa-calendar-o"></i><span class="hide-menu">Holidays</span></a></li>
                <li><a href="<?php echo @constant('site_url');?>
/admin/accounts" aria-expanded="false"><i class="fa fa-cogs"></i><span class="hide-menu">System Admins</span></a></li>
                <li class="nav-label">System</li>
                <li><a href="javascript:void(0)" data-iziModal-open="#configurations"><i class="fa fa-cogs"></i><span class="hide-menu">Configurations</span></a></li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</div>
<!-- End Left Sidebar  --><?php }
}
