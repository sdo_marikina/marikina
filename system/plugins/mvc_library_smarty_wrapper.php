<?php
/**
 * Smarty Library
 */

if(!defined("SMARTY_SPL_AUTOLOAD"))
    define('SMARTY_SPL_AUTOLOAD', 1);

require("system/libraries/smarty/Autoloader.php");

Smarty_Autoloader::register();
 
class MVC_Library_Smarty_Wrapper Extends Smarty {
  function __construct(){
    parent::__construct();
    $this->setTemplateDir('templates/');
    $this->setCompileDir('system/data/smarty/templates_c/');
    $this->setConfigDir('system/data/smarty/configs/');
    $this->setCacheDir('system/data/smarty/cache/');
  }
}