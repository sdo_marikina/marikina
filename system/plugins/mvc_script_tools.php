<?php
function go($data){
  return header("location: ".$data);
}

function date_range($first, $last, $step = '+1 day', $output_format = 'd/m/Y', $weekends = false){
    $dates = array();
    $current = strtotime($first);
    $last = strtotime($last);
    while( $current <= $last ){
      if(!$weekends){
        if(!in_array(date("D", $current), array("Sun", "Sat"))){
          $dates[] = date($output_format, $current);
        }
      } else {
        $dates[] = date($output_format, $current);
      }
      $current = strtotime($step, $current);
    }
    return $dates;
}

function moveFingerprint($data){
  move_uploaded_file($data['tmp_name'], "uploads/fingerprints/" . basename($data["name"]));
  return basename($data["name"]);
}

function movePicture($data){
  move_uploaded_file($data['tmp_name'], "uploads/access/" . basename($data["name"]));
  return basename($data["name"]);
}

function pagination($item_count, $limit, $cur_page, $link){
  $page_count = ceil($item_count/$limit) - 1;
  $current_range = array(($cur_page - 2 < 1 ? 1 : $cur_page - 2), ($cur_page + 2 > $page_count ? $page_count : $cur_page + 2));
  $first = '<li><a href="'.sprintf($link, '1').'">1</a></li>';
  $last = ($cur_page > $page_count - 4 ? null : null).'<li><a href="'.sprintf($link, $page_count).'">'.$page_count.'</a></li>';
  $first_page = $cur_page > 3 ? $first : null;
  $last_page = $cur_page < $page_count - 2 ? $last : null;
  $previous_page = $cur_page > 1 ? '<li><a href="'.sprintf($link, ($cur_page-1)).'">Previous</a></li>' : null;
  $next_page = $cur_page < $page_count ? '<li><a href="'.sprintf($link, ($cur_page+1)).'">Next</a></li>' : null;
  for ($x = $current_range[0]; $x <= $current_range[1]; ++$x){
    $active = '<li class="active"><a href="'.sprintf($link, $x).'">'.$x.'</a></li>';
    $unactive = '<li><a href="'.sprintf($link, $x).'">'.$x.'</a></li>';
    $pages[] = $x == $cur_page ? $active : $unactive;
  }
  if ($page_count > 1)
    return '<ul class="pagination pagination-lg">'.$previous_page.$first_page.implode($pages).$last_page.$next_page.'</ul>';
}

function prepend($string, $orig_filename) {
  $context = stream_context_create();
  $orig_file = fopen($orig_filename, 'r', 1, $context);
  $temp_filename = tempnam(sys_get_temp_dir(), 'php_prepend_');
  file_put_contents($temp_filename, $string);
  file_put_contents($temp_filename, $orig_file, FILE_APPEND);
  fclose($orig_file);
  unlink($orig_filename);
  rename($temp_filename, $orig_filename);
}

function manualMark($data){
  if($data["manual"] == 1){
    return "*";
  } else {
    return "";
  }
}

function cleanLog($fileName, $max = 10) {
    $file = array_filter(array_map("trim", file($fileName)));
    $file = array_slice($file, 0, $max);
    count($file) > $max and array_shift($file);
    file_put_contents($fileName, implode(PHP_EOL, array_filter($file)));
}

function wash($data){
  return filter_var($data, FILTER_SANITIZE_STRING);
}

function washEmail($data){
  return filter_var($data, FILTER_SANITIZE_EMAIL);
}

function washURL($data){
  return filter_var($data, FILTER_SANITIZE_URL);
}

function isInt($data){
  if(filter_var($data, FILTER_VALIDATE_INT) === 0 || !filter_var($data, FILTER_VALIDATE_INT) === false) {
    return true;
  } else {
    return false;
  }
}

function isEmail($data){
  if(!filter_var($data, FILTER_VALIDATE_EMAIL) === false) {
    return true;
  } else {
    return false;
  }
}

function isURL($data){
  if(!filter_var($data, FILTER_VALIDATE_URL) === false) {
    return true;
  } else {
    return false;
  }
}