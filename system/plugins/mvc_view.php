<?php
/**
 * Name: NTEK MVC FRAMEWORK
 * About: Private MVC Framework of NTEK Systems
 * Copyright: (C) 2018, All Rights Reserved.
 * Author: Norielle Cruz <nogats07@gmail.com>
 */

/**
 * MVC_View
 * @package MVC
 * @author Monte Ohrt
 */

class MVC_View {

 	/**
	 * $view_vars
	 * vars for view file assignment
	 * @access	public
	 */
	
  var $view_vars = array();
  
 	/**
	 * Class constructor
	 * @access	public
	 */
	
  function __construct() {}
  
	/**
	 * Assign
	 * Assign view variables
	 * @access public
	 * @param mixed $key key of assignment, or value to assign
	 * @param mixed $value value of assignment
	 */    
	
  public function assign($key, $value=null){
    if(isset($value))
      $this->view_vars[$key] = $value;
    else
      foreach($key as $k => $v)
        if(is_int($k))
          $this->view_vars[] = $v;
        else
          $this->view_vars[$k] = $v;
  }  

	/**
	 * Display
	 * Display a view file
	 * @access public
	 * @param string $filename the name of the view file
	 * @return boolean
	 */    
	
  public function display($_mvc_filename,$view_vars=null){
    return $this->_view("{$_mvc_filename}.php",$view_vars);
  }  

	/**
	 * Fetch
	 * Return the contents of a view file
	 * @access public
	 * @param string $filename
	 * @return string contents of view
	 */    
	
  public function fetch($filename,$view_vars=null){
    ob_start();
    $this->display($filename,$view_vars);
    $results = ob_get_contents();
    ob_end_clean();
    return $results;
  }  

	/**
	 * Sysview
	 * Internal: view a system file
	 * @access private
	 * @param string $filename
	 * @return boolean
	 */    
	
  public function sysview($filename,$view_vars = null){
    $filepath = "{$filename}.php";
    return $this->_view($filepath,$view_vars);
  }

	/**
	 * _view
	 * Internal: display a view file
	 * @access public
	 * @param string $_mvc_filepath
   * @param array $view_vars
	 */   
	
  public function _view($_mvc_filepath,$view_vars = null){
    // bring view vars into view scope
    extract($this->view_vars);
    if(isset($view_vars))
      extract($view_vars);
    try {
      include($_mvc_filepath);
    } catch (Exception $e) {
      throw new Exception("Unknown file '$_mvc_filepath'");      
    }
  }
}