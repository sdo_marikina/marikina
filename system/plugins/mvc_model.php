<?php
/**
 * Name: NTEK MVC FRAMEWORK
 * About: Private MVC Framework of NTEK Systems
 * Copyright: (C) 2018, All Rights Reserved.
 * Author: Norielle Cruz <nogats07@gmail.com>
 */

/**
 * MVC_Model
 * @package MVC
 * @author Norielle Cruz <nogats07@gmail.com>
 */

class MVC_Model {
 	/**
	 * $db
	 * The database object instance
	 * @access public
	 */ 
	
  var $db = null;  
    
 	/**
	 * Class constructor
	 * @access public
	 */
	
  function __construct($poolname=null) {
    $this->db = mvc::instance()->controller->load->database($poolname);
  }  
}