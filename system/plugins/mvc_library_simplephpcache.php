<?php
/**
 * SimplePHPCache Library
 */

if(!defined("SMARTY_SPL_AUTOLOAD"))
  define('SMARTY_SPL_AUTOLOAD', 1);

require("system/libraries/simplephpcache/SimplePHPCache.class.php");
 
class MVC_Library_SimplePHPCache Extends Cache {
  function __construct(){
    parent::__construct();
    $this->setCachePath("system/data/cache/");
  }
}