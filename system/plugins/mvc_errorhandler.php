<?php
/**
 * Name: NTEK MVC FRAMEWORK
 * About: Private MVC Framework of NTEK Systems
 * Copyright: (C) 2018, All Rights Reserved.
 * Author: Norielle Cruz <nogats07@gmail.com>
 */

/**
 * MVC_ErrorHandler
 * A simple exception handler to display exceptions in a formatted box
 * @package MVC
 * @author Norielle Cruz <nogats07@gmail.com>
 */

function MVC_ErrorHandler($errno, $errstr, $errfile, $errline) {
  // do nothing if error reporting is turned off
  if (error_reporting() === 0){
    return;
  }
  // be sure received error is supposed to be reported
  if (error_reporting() & $errno){   
		throw new MVC_ExceptionHandler($errstr, $errno, $errno, $errfile, $errline);
  }
}