<?php
/**
 * Name: NTEK MVC FRAMEWORK
 * About: Private MVC Framework of NTEK Systems
 * Copyright: (C) 2018, All Rights Reserved.
 * Author: Norielle Cruz <nogats07@gmail.com> 
 */


/**
 * MVC_Controller
 * @package	MVC
 * @author Norielle Cruz <nogats07@gmail.com>
 */

class MVC_Controller {

 	/**
	 * Class constructor
	 * @access public
	 */

	function __construct(){
    /* save controller instance */
    mvc::instance($this,'controller');
  
    /* instantiate load library */
    $this->load = new MVC_Load;  

    /* instantiate view library */
    $this->view = new MVC_View;
  }
  
	/**
	 * Index
	 * The default controller method
	 * @access public
	 */    
	
  function index() { }

	/**
	 * __call
	 * Gets called when an unspecified method is used
	 * @access public
	 */    
  function __call($function, $args) {
    throw new Exception($this->index());
  }
}