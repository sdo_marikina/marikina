define({ "api": [
  {
    "type": "GET",
    "url": "/api/departments",
    "title": "Departments",
    "description": "<p>Get the list of departments</p>",
    "name": "Departments",
    "group": "Smartentry",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success Response:",
          "content": "{\n\"result\": \"success\",\n\"data\": [\n     {\n         \"id\": \"2\",\n         \"name\": \"Office\"\n     },\n     {\n         \"id\": \"1\",\n         \"name\": \"Registrar\"\n     }\n ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "files/api.php",
    "groupTitle": "Smartentry"
  },
  {
    "type": "GET",
    "url": "/api/access",
    "title": "Employee Access",
    "description": "<p>Record the employee access time-in or time-out</p>",
    "name": "Employee_Access",
    "group": "Smartentry",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "employee_no",
            "description": "<p>Employee unique number.</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "access_type",
            "description": "<p>Access type. <br/> 1 = Time-in <br/> 2 = Time-out</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "SuccessResponse": [
          {
            "group": "SuccessResponse",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>Result handler of the API.</p>"
          },
          {
            "group": "SuccessResponse",
            "type": "String",
            "optional": false,
            "field": "timestamp",
            "description": "<p>The timestamp of insertion to database.</p>"
          },
          {
            "group": "SuccessResponse",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>The result message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success Response:",
          "content": "{\n \"result\": \"success\",\n \"timestamp\": \"2018-07-22 19:23:32\",\n \"data\": \"Access has been recorded!\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "FailResponse": [
          {
            "group": "FailResponse",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>Result handler of the API.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Fail Response:",
          "content": "{\n \"result\": \"fail\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "files/api.php",
    "groupTitle": "Smartentry"
  },
  {
    "type": "GET",
    "url": "/api/employee",
    "title": "Employee Verification",
    "description": "<p>Verify the employee RFID or Fingerprint</p>",
    "name": "Employee_Verification",
    "group": "Smartentry",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "type",
            "description": "<p>Verification type. <br/> 1 = RFID <br/> 2 = Fingerprint <br/> 3 = Employee Number</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>Verification id of RFID, Fingerprint or Employee Number.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "SuccessResponse": [
          {
            "group": "SuccessResponse",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>Result handler of the API.</p>"
          },
          {
            "group": "SuccessResponse",
            "type": "String",
            "optional": false,
            "field": "data",
            "description": "<p>Employee details.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success Response:",
          "content": "{\n \"result\": \"success\",\n \"data\": {\n   \"id\": \"9\",\n   \"employee_no\": \"1241241244\",\n   \"rfid\": \"1234567\",\n   \"type\": \"1\",\n   \"basic_pay\": \"10000\",\n   \"department\": \"1\",\n   \"fname\": \"Test\",\n   \"mname\": \"Moe\",\n   \"lname\": \"Doe\",\n   \"fid\": \"23\",\n   \"ftemplate\": \"2038403_template.dat\"\n }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "FailResponse": [
          {
            "group": "FailResponse",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>Result handler of the API.</p>"
          },
          {
            "group": "FailResponse",
            "type": "String",
            "optional": false,
            "field": "data",
            "description": "<p>Employee details.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Fail Response:",
          "content": "{\n \"result\": \"fail\",\n       \"data\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "files/api.php",
    "groupTitle": "Smartentry"
  },
  {
    "type": "GET",
    "url": "/api/types",
    "title": "Employment Types",
    "description": "<p>Get the list of employment types</p>",
    "name": "Employment_Types",
    "group": "Smartentry",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success Response:",
          "content": "{\n\t \"result\": \"success\",\n \"data\": [\n     {\n         \"id\": \"1\",\n         \"name\": \"Non-Teaching\"\n     },\n     {\n         \"id\": \"2\",\n        \"name\": \"Teaching\"\n     },\n     {\n         \"id\": \"3\",\n         \"name\": \"Division\"\n     }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "files/api.php",
    "groupTitle": "Smartentry"
  },
  {
    "type": "GET",
    "url": "/api/positions",
    "title": "Positions",
    "description": "<p>Get the list of positions</p>",
    "name": "Positions",
    "group": "Smartentry",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "Success Response:",
          "content": "{\n\"result\": \"success\",\n\"data\": [\n     {\n         \"id\": \"2\",\n         \"name\": \"Security\"\n     },\n     {\n         \"id\": \"1\",\n         \"name\": \"CEO\"\n     }\n ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "files/api.php",
    "groupTitle": "Smartentry"
  },
  {
    "type": "POST",
    "url": "/api/register",
    "title": "Register",
    "description": "<p>Register a new employee to the database</p>",
    "name": "Register",
    "group": "Smartentry",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "employee_no",
            "description": "<p>Employee unique number.</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "type",
            "description": "<p>Employee type.</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "basic_pay",
            "description": "<p>Employee salary.</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "department",
            "description": "<p>Department of the employee.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "fname",
            "description": "<p>Employee first name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mname",
            "description": "<p>Employee middle name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lname",
            "description": "<p>Employee last name.</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "rfid",
            "description": "<p>Employee card number.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "finger_print_id",
            "description": "<p>Fingerprint unqiue id.</p>"
          },
          {
            "group": "Parameter",
            "type": "FILE",
            "optional": false,
            "field": "finger_print_template",
            "description": "<p>Fingerprint template file.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "SuccessResponse": [
          {
            "group": "SuccessResponse",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>Result handler of the API.</p>"
          },
          {
            "group": "SuccessResponse",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>The result message</p>"
          }
        ],
        "FailResponse": [
          {
            "group": "FailResponse",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>Result handler of the API.</p>"
          },
          {
            "group": "FailResponse",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>The result message</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success Response:",
          "content": "{\n \"result\": \"success\",\n \"message\": \"Employee has been successfully added!\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Fail Response:",
          "content": "{\n \"result\": \"fail\",\n \"message\": \"RFID is already used by existing employee!\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "files/api.php",
    "groupTitle": "Smartentry"
  },
  {
    "type": "POST",
    "url": "/api/update",
    "title": "Update",
    "description": "<p>Update employee rfid or fingerprint</p>",
    "name": "Update",
    "group": "Smartentry",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "employee_no",
            "description": "<p>Employee unique number.</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "type",
            "description": "<p>Update type.</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>rfid or fingerprint id.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "SuccessResponse": [
          {
            "group": "SuccessResponse",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>Result handler of the API.</p>"
          },
          {
            "group": "SuccessResponse",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>The result message</p>"
          }
        ],
        "FailResponse": [
          {
            "group": "FailResponse",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>Result handler of the API.</p>"
          },
          {
            "group": "FailResponse",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>The result message</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success Response:",
          "content": "{\n \"result\": \"success\",\n \"message\": \"Employee RFID has been updated!\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Fail Response:",
          "content": "{\n \"result\": \"fail\",\n \"message\": \"Something went wrong!\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "files/api.php",
    "groupTitle": "Smartentry"
  },
  {
    "type": "POST",
    "url": "/api/uploadImage",
    "title": "Upload Image",
    "description": "<p>Upload captured image of employee when rfid access was used</p>",
    "name": "Upload_Image",
    "group": "Smartentry",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "employee_no",
            "description": "<p>Employee unique number.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "date",
            "description": "<p>Access Date.</p>"
          },
          {
            "group": "Parameter",
            "type": "FILE",
            "optional": false,
            "field": "picture",
            "description": "<p>Captured image file.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "SuccessResponse": [
          {
            "group": "SuccessResponse",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>Result handler of the API.</p>"
          },
          {
            "group": "SuccessResponse",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>The result message</p>"
          }
        ],
        "FailResponse": [
          {
            "group": "FailResponse",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>Result handler of the API.</p>"
          },
          {
            "group": "FailResponse",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>The result message</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success Response:",
          "content": "{\n \"result\": \"success\",\n \"message\": \"Captured image has been uploaded!\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Fail Response:",
          "content": "{\n \"result\": \"fail\",\n \"message\": \"No access log have matched the employee number and timestamp!\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "files/api.php",
    "groupTitle": "Smartentry"
  }
] });
