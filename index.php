<?php
/**
 * NAMS - NTEK Attendance Management System
 * @version 1.0
 * @author Norielle Cruz <nogats07@gmail.com>
 */

    /**
     * Environment configuration
     */



    session_start();
    define("site_url", "http://{$_SERVER['HTTP_HOST']}/marikina");

    /**
     * Error reporting level
     */
     
    error_reporting(E_ALL);
    
    /**
     * System directory
     */
     
    define('MVC_BASEDIR', 'system/');
    
    /**
     * Error handler
     */
     
    define('MVC_ERROR_HANDLING', 0);
    
    /**
     * Directory separator
     */
    
    define('DS', DIRECTORY_SEPARATOR);
    
    /**
     * MVC class
     */
     
    require(MVC_BASEDIR . 'init.php');
    
    /**
     * Initialize
     */
     
    $mvc = new mvc();
    
    /**
     * Run system
     */
     
    $mvc->main();
