$(function() {
    var oTable = $('#access-logs').DataTable({
        ajax: site_url + "/ajax/access",
        dom: 'B<"clear">lfrtip',
        lengthMenu: [10, 25, 50, 75, 100],
        pageLength: 50,
        aaSorting: [[0, "desc"]],
        sPaginationType: "full_numbers",
        liveAjax: {
            interval: 15000
        },
        oLanguage: {
            sSearch: "<i class='fa fa-search'></i> Search",
            sSearchPlaceholder: "Name, Card ID, Access Type"
        },
        columns: [
            {
                title: 'ID',
                data:  'id',
                searchable: false,
                visible: false
            },
            {
                title: 'Employee Number',
                width: '13%',
                data:  'eid'
            },
            {
                title: 'Card ID',
                width: '12%',
                data:  'card'
            },
            {
                title: 'First Name',
                data:  'fname'
            },
            {
                title: 'Middle Name',
                data:  'mname'
            },
            {
                title: 'Last Name',
                data:  'lname'
            },
            {
                title: 'Access Type',
                data:  'access'
            },
            {
                title: 'Date',
                data:  'date'
            },
            {
                title: 'Date Filter',
                data:  'fdate',
                visible: false
            }
        ],
        buttons: [
            {
                extend: 'csvHtml5',
                className: "btn-sm",
                text: "<i class='fa fa-table'></i> CSV",
                exportOptions: {
                    columns: [1, 2, 3, 4, 5, 6, 7]
                },
                action: function(e, dt, button, config) {
                    swal({
                        title: "Exporting",
                        text: "Please wait while the system exports the access log",
                        icon: "info",
                        closeOnClickOutside: false,
                        closeOnEsc: false,
                        button: false
                    });
                    setTimeout(function(){
                        config.filename = "Access Logs " + date.getMonth() + "-" + date.getDate() + "-" + date.getFullYear();
                        $.fn.dataTable.ext.buttons.csvHtml5.action(e, dt, button, config);
                        swal({
                            title: "Success!",
                            text: "Access log has been exported to CSV!",
                            icon: "success",
                            button: false,
                            timer: 3000
                        });
                    }, 20000);
                }
            },
            {
                extend: 'excelHtml5',
                className: "btn-sm",
                text: "<i class='fa fa-file-excel-o'></i> Excel",
                exportOptions: {
                    columns: [1, 2, 3, 4, 5, 6, 7]
                },
                action: function(e, dt, button, config) {
                    swal({
                        title: "Exporting",
                        text: "Please wait while the system exports the access log",
                        icon: "info",
                        closeOnClickOutside: false,
                        closeOnEsc: false,
                        button: false
                    });
                    setTimeout(function(){
                        config.filename = "Access Logs " + date.getMonth() + "-" + date.getDate() + "-" + date.getFullYear();
                        $.fn.dataTable.ext.buttons.excelHtml5.action(e, dt, button, config);
                        swal({
                            title: "Success!",
                            text: "Access log has been exported to EXCEL!",
                            icon: "success",
                            button: false,
                            timer: 3000
                        });
                    }, 20000);
                }
            },
            /*
            {
                extend: 'pdfHtml5',
                className: "btn-sm",
                text: "<i class='fa fa-file'></i> PDF",
                exportOptions: {
                    columns: [1, 2, 3]
                },
                action: function(e, dt, button, config) {
                    swal({
                        title: "Exporting",
                        text: "Please wait while the system exports the access log",
                        icon: "info",
                        closeOnClickOutside: false,
                        closeOnEsc: false,
                        button: false
                    });
                    setTimeout(function(){
                        config.filename = "Access Logs " + date.getMonth() + "-" + date.getDate() + "-" + date.getFullYear();
                        $.fn.dataTable.ext.buttons.pdfHtml5.action(e, dt, button, config);
                        swal({
                            title: "Success!",
                            text: "Access log has been exported to PDF!",
                            icon: "success",
                            button: false,
                            timer: 3000
                        });
                    }, 10000);
                }
            },
            */
            {
                extend: "print",
                className: "btn-sm",
                text: "<i class='fa fa-print'></i> Print",
                exportOptions: {
                    columns: [1, 2, 3, 4, 5, 6, 7]
                }
            },
            {
                extend: "copyHtml5",
                className: "btn-sm",
                text: "<i class='fa fa-clipboard'></i> Copy to Clipboard",
                exportOptions: {
                    columns: [1, 2, 3, 4, 5, 6, 7]
                }
            }
        ]
    });

    $("#date-min").datepicker({
        "onSelect": function(date) {
            $(this).val(date);
            minDateFilter = new Date(date).getTime();
            oTable.draw();
        }
    }).keyup(function() {
        $(this).val(date);
        minDateFilter = new Date(this.value).getTime();
        oTable.draw();
    });

    $("#date-max").datepicker({
        "onSelect": function(date) {
            $(this).val(date);
            maxDateFilter = new Date(date).getTime();
            oTable.draw();
        }
    }).keyup(function() {
        $(this).val(date);
        maxDateFilter = new Date(this.value).getTime();
        oTable.draw();
    });
});

minDateFilter = "";
maxDateFilter = "";

$.fn.dataTableExt.afnFiltering.push(
    function(oSettings, aData, iDataIndex) {
        if (typeof aData._date == 'undefined') {
            aData._date = new Date(aData[8]).getTime();
        }

        if (minDateFilter && !isNaN(minDateFilter)) {
            if (aData._date < minDateFilter) {
                return false;
            }
        }

        if (maxDateFilter && !isNaN(maxDateFilter)) {
            if (aData._date > maxDateFilter) {
                return false;
            }
        }

        return true;
    }
);