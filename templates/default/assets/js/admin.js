$(document).ready(function(){
  $(".iziModal").iziModal({
     padding: 15,
     focusInput: false,
     onOpening: function(){
        $("#bypass-date").datepicker();
        $("#dtr-start").datepicker();
        $("#dtr-end").datepicker();
        $("#form7-start").datepicker();
        $("#form7-end").datepicker();
        $("#holiday-date").datepicker({changeYear: false, dateFormat: 'mm-dd'});
     }
  });

  $('.timepicker').timepicker({
    timeFormat: 'h:mm p',
    interval: 5,
    minTime: '6',
    maxTime: '7:00PM',
    startTime: '6',
    dynamic: false,
    dropdown: true,
    scrollbar: true
  });

  $('[data-toggle="tooltip"]').tooltip()

  $("#holiday-date").datepicker({changeYear: false, dateFormat: 'mm-dd'});

  $("#add-employee-form").submit(function(e){
    e.preventDefault();
    var data = new FormData(this);
    $.ajax({
        url: site_url + "/ajax/add_employee",
        type: "POST",
        data: data,
        contentType: false,
        processData: false,
        beforeSend: function(){
            if(data.get("eid").length < 1){
                swal({
                    title: "Error!",
                    text: "Employee number cannot be empty!",
                    icon: "warning",
                    button: false,
                    timer: 3000
                });
                return false;
            }
            if(data.get("basic").length < 1){
                swal({
                    title: "Error!",
                    text: "Basic pay cannot be empty!",
                    icon: "warning",
                    button: false,
                    timer: 3000
                });
                return false;
            }
            $("button").attr("disabled", "");
        },
        success: function(result){
            $("button").removeAttr("disabled");
            var data = JSON.parse(result);
            if(data.result == "success"){
                swal({
                    title: "Success!",
                    text: data.message,
                    icon: "success",
                    timer: 3000,
                    closeOnClickOutside: false,
                    closeOnEsc: false,
                    button: false
                });
                $('.iziModal').iziModal('close');
                setInterval(function() {
                    window.location = site_url + "/admin/employees";
                }, 3000);
            } else {
                swal({
                    title: "Error!",
                    text: data.message,
                    icon: "error",
                    button: false,
                    timer: 3000
                });
            }
        }
    });
  });

  $("#add-position-form").submit(function(e){
    e.preventDefault();
    var data = new FormData(this);
    $.ajax({
        url: site_url + "/ajax/add_position",
        type: "POST",
        data: data,
        contentType: false,
        processData: false,
        beforeSend: function(){
            if(data.get("name").length < 1){
                swal({
                    title: "Error!",
                    text: "Position name cannot be empty!",
                    icon: "warning",
                    button: false,
                    timer: 3000
                });
                return false;
            }
            if(data.get("description").length < 1){
                swal({
                    title: "Error!",
                    text: "Position description cannot be empty!",
                    icon: "warning",
                    button: false,
                    timer: 3000
                });
                return false;
            }
            $("button").attr("disabled", "");
        },
        success: function(result){
            $("button").removeAttr("disabled");
            var data = JSON.parse(result);
            if(data.result == "success"){
                swal({
                    title: "Success!",
                    text: data.message,
                    icon: "success",
                    timer: 5000,
                    button: false
                });
                $('.iziModal').iziModal('close');
            } else {
                swal({
                    title: "Error!",
                    text: data.message,
                    icon: "error",
                    button: false,
                    timer: 3000
                });
            }
        }
    });
  });

  $("#add-department-form").submit(function(e){
    e.preventDefault();
    var data = new FormData(this);
    $.ajax({
        url: site_url + "/ajax/add_department",
        type: "POST",
        data: data,
        contentType: false,
        processData: false,
        beforeSend: function(){
            if(data.get("name").length < 1){
                swal({
                    title: "Error!",
                    text: "Department name cannot be empty!",
                    icon: "warning",
                    button: false,
                    timer: 3000
                });
                return false;
            }
            if(data.get("description").length < 1){
                swal({
                    title: "Error!",
                    text: "Department description cannot be empty!",
                    icon: "warning",
                    button: false,
                    timer: 3000
                });
                return false;
            }
            $("button").attr("disabled", "");
        },
        success: function(result){
            $("button").removeAttr("disabled");
            var data = JSON.parse(result);
            if(data.result == "success"){
                swal({
                    title: "Success!",
                    text: data.message,
                    icon: "success",
                    timer: 5000,
                    button: false
                });
                $('.iziModal').iziModal('close');
            } else {
                swal({
                    title: "Error!",
                    text: data.message,
                    icon: "error",
                    button: false,
                    timer: 3000
                });
            }
        }
    });
  });

  $("#add-holiday-form").submit(function(e){
    e.preventDefault();
    var data = new FormData(this);
    $.ajax({
        url: site_url + "/ajax/add_holiday",
        type: "POST",
        data: data,
        contentType: false,
        processData: false,
        beforeSend: function(){
            if(data.get("name").length < 1){
                swal({
                    title: "Error!",
                    text: "Holiday name cannot be empty!",
                    icon: "warning",
                    button: false,
                    timer: 3000
                });
                return false;
            }
            if(data.get("holiday").length < 1){
                swal({
                    title: "Error!",
                    text: "Holiday date cannot be empty!",
                    icon: "warning",
                    button: false,
                    timer: 3000
                });
                return false;
            }
            $("button").attr("disabled", "");
        },
        success: function(result){
            $("button").removeAttr("disabled");
            var data = JSON.parse(result);
            if(data.result == "success"){
                swal({
                    title: "Success!",
                    text: data.message,
                    icon: "success",
                    timer: 5000,
                    button: false
                });
                $('.iziModal').iziModal('close');
            } else {
                swal({
                    title: "Error!",
                    text: data.message,
                    icon: "error",
                    button: false,
                    timer: 3000
                });
            }
        }
    });
  });

  $("#add-account-form").submit(function(e){
    e.preventDefault();
    var data = new FormData(this);
    $.ajax({
        url: site_url + "/ajax/add_account",
        type: "POST",
        data: data,
        contentType: false,
        processData: false,
        beforeSend: function(){
            if(data.get("username").length < 1){
                swal({
                    title: "Error!",
                    text: "Username cannot be empty!",
                    icon: "warning",
                    button: false,
                    timer: 3000
                });
                return false;
            }
            if(data.get("password").length < 1){
                swal({
                    title: "Error!",
                    text: "Password cannot be empty!",
                    icon: "warning",
                    button: false,
                    timer: 3000
                });
                return false;
            }
            $("button").attr("disabled", "");
        },
        success: function(result){
            $("button").removeAttr("disabled");
            var data = JSON.parse(result);
            if(data.result == "success"){
                swal({
                    title: "Success!",
                    text: data.message,
                    icon: "success",
                    timer: 5000,
                    button: false
                });
                $('.iziModal').iziModal('close');
            } else {
                swal({
                    title: "Error!",
                    text: data.message,
                    icon: "error",
                    button: false,
                    timer: 3000
                });
            }
        }
    });
  });

  $("#edit-employee-form").submit(function(e){
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
          url: site_url + "/ajax/edit_employee",
          type: "POST",
          data: data,
          contentType: false,
          processData: false,
          beforeSend: function(){
            if(data.get("fname").length < 1){
                  swal({
                      title: "Error!",
                      text: "Employee first name cannot be empty!",
                      icon: "warning",
                      button: false,
                      timer: 3000
                  });
                  return false;
              }
              if(data.get("mname").length < 1){
                  swal({
                      title: "Error!",
                      text: "Employee middle name cannot be empty!",
                      icon: "warning",
                      button: false,
                      timer: 3000
                  });
                  return false;
              }
              if(data.get("lname").length < 1){
                  swal({
                      title: "Error!",
                      text: "Employee last name cannot be empty!",
                      icon: "warning",
                      button: false,
                      timer: 3000
                  });
                  return false;
              }
              if(data.get("basic").length < 1){
                  swal({
                      title: "Error!",
                      text: "Employee basic pay cannot be empty!",
                      icon: "warning",
                      button: false,
                      timer: 3000
                  });
                  return false;
              }
              $("button").attr("disabled", "");
          },
          success: function(result){
              $("button").removeAttr("disabled");
              var data = JSON.parse(result);
              if(data.result == "success"){
                  swal({
                      title: "Success!",
                      text: data.message,
                      icon: "success",
                      closeOnClickOutside: false,
                      closeOnEsc: false,
                      button: false
                  });
                  setInterval(function() {
                      location.reload(true);
                  }, 3000);
              } else {
                  swal({
                      title: "Error!",
                      text: data.message,
                      icon: "error",
                      button: false,
                      timer: 3000
                  });
              }
          }
        });
  });

  $("#edit-position-form").submit(function(e){
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
          url: site_url + "/ajax/edit_position",
          type: "POST",
          data: data,
          contentType: false,
          processData: false,
          beforeSend: function(){
            if(data.get("name").length < 1){
                  swal({
                      title: "Error!",
                      text: "Position name cannot be empty!",
                      icon: "warning",
                      button: false,
                      timer: 3000
                  });
                  return false;
              }
              if(data.get("description").length < 1){
                  swal({
                      title: "Error!",
                      text: "Position description cannot be empty!",
                      icon: "warning",
                      button: false,
                      timer: 3000
                  });
                  return false;
              }
              $("button").attr("disabled", "");
          },
          success: function(result){
              $("button").removeAttr("disabled");
              var data = JSON.parse(result);
              if(data.result == "success"){
                  swal({
                      title: "Success!",
                      text: data.message,
                      icon: "success",
                      closeOnClickOutside: false,
                      closeOnEsc: false,
                      button: false
                  });
                  setInterval(function() {
                      location.reload(true);
                  }, 3000);
              } else {
                  swal({
                      title: "Error!",
                      text: data.message,
                      icon: "error",
                      button: false,
                      timer: 3000
                  });
              }
          }
        });
  });

  $("#edit-department-form").submit(function(e){
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
          url: site_url + "/ajax/edit_department",
          type: "POST",
          data: data,
          contentType: false,
          processData: false,
          beforeSend: function(){
            if(data.get("name").length < 1){
                  swal({
                      title: "Error!",
                      text: "Department name cannot be empty!",
                      icon: "warning",
                      button: false,
                      timer: 3000
                  });
                  return false;
              }
              if(data.get("description").length < 1){
                  swal({
                      title: "Error!",
                      text: "Department description cannot be empty!",
                      icon: "warning",
                      button: false,
                      timer: 3000
                  });
                  return false;
              }
              $("button").attr("disabled", "");
          },
          success: function(result){
              $("button").removeAttr("disabled");
              var data = JSON.parse(result);
              if(data.result == "success"){
                  swal({
                      title: "Success!",
                      text: data.message,
                      icon: "success",
                      closeOnClickOutside: false,
                      closeOnEsc: false,
                      button: false
                  });
                  setInterval(function() {
                      location.reload(true);
                  }, 3000);
              } else {
                  swal({
                      title: "Error!",
                      text: data.message,
                      icon: "error",
                      button: false,
                      timer: 3000
                  });
              }
          }
        });
  });

  $("#edit-holiday-form").submit(function(e){
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
          url: site_url + "/ajax/edit_holiday",
          type: "POST",
          data: data,
          contentType: false,
          processData: false,
          beforeSend: function(){
            if(data.get("name").length < 1){
                  swal({
                      title: "Error!",
                      text: "Holiday name cannot be empty!",
                      icon: "warning",
                      button: false,
                      timer: 3000
                  });
                  return false;
              }
              if(data.get("holiday").length < 1){
                  swal({
                      title: "Error!",
                      text: "Holiday date cannot be empty!",
                      icon: "warning",
                      button: false,
                      timer: 3000
                  });
                  return false;
              }
              $("button").attr("disabled", "");
          },
          success: function(result){
              $("button").removeAttr("disabled");
              var data = JSON.parse(result);
              if(data.result == "success"){
                  swal({
                      title: "Success!",
                      text: data.message,
                      icon: "success",
                      closeOnClickOutside: false,
                      closeOnEsc: false,
                      button: false
                  });
                  setInterval(function() {
                      location.reload(true);
                  }, 3000);
              } else {
                  swal({
                      title: "Error!",
                      text: data.message,
                      icon: "error",
                      button: false,
                      timer: 3000
                  });
              }
          }
        });
  });

  $("#edit-account-form").submit(function(e){
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
          url: site_url + "/ajax/edit_account",
          type: "POST",
          data: data,
          contentType: false,
          processData: false,
          beforeSend: function(){
            if(data.get("username").length < 1){
                  swal({
                      title: "Error!",
                      text: "Username cannot be empty!",
                      icon: "warning",
                      button: false,
                      timer: 3000
                  });
                  return false;
              }
              $("button").attr("disabled", "");
          },
          success: function(result){
              $("button").removeAttr("disabled");
              var data = JSON.parse(result);
              if(data.result == "success"){
                  swal({
                      title: "Success!",
                      text: data.message,
                      icon: "success",
                      closeOnClickOutside: false,
                      closeOnEsc: false,
                      button: false
                  });
                  setInterval(function() {
                      location.reload(true);
                  }, 3000);
              } else {
                  swal({
                      title: "Error!",
                      text: data.message,
                      icon: "error",
                      button: false,
                      timer: 3000
                  });
              }
          }
        });
  });

  $("#submit-bypass-form").submit(function(e){
    e.preventDefault();
    var data = new FormData(this);
    $.ajax({
        url: site_url + "/ajax/bypass",
        type: "POST",
        data: data,
        contentType: false,
        processData: false,
        beforeSend: function(){
            if(data.get("eid").length < 1){
                swal({
                    title: "Error!",
                    text: "Employee numbers cannot be empty!",
                    icon: "warning",
                    button: false,
                    timer: 3000
                });
                return false;
            }
            if(data.get("date").length < 1){
                swal({
                    title: "Error!",
                    text: "Access date cannot be empty!",
                    icon: "warning",
                    button: false,
                    timer: 3000
                });
                return false;
            }
            if(data.get("time").length < 1){
                swal({
                    title: "Error!",
                    text: "Access time cannot be empty!",
                    icon: "warning",
                    button: false,
                    timer: 3000
                });
                return false;
            }
            $("button").attr("disabled", "");
        },
        success: function(result){
            $("button").removeAttr("disabled");
            var data = JSON.parse(result);
            if(data.result == "success"){
                swal({
                    title: "Success!",
                    text: data.message,
                    icon: "success",
                    timer: 5000,
                    button: false
                });
                $('.iziModal').iziModal('close');
            } else {
                swal({
                    title: "Error!",
                    text: data.message,
                    icon: "error",
                    button: false,
                    timer: 3000
                });
            }
        }
    });
  });

  $("#submit-dtr-form").submit(function(e){
    e.preventDefault();
    var data = new FormData(this);
    $.ajax({
        url: site_url + "/ajax/dtr",
        type: "POST",
        data: data,
        contentType: false,
        processData: false,
        beforeSend: function(){
            if(data.get("start").length < 1){
                swal({
                    title: "Error!",
                    text: "Start date cannot be empty!",
                    icon: "warning",
                    button: false,
                    timer: 3000
                });
                return false;
            }
            if(data.get("end").length < 1){
                swal({
                    title: "Error!",
                    text: "End date cannot be empty!",
                    icon: "warning",
                    button: false,
                    timer: 3000
                });
                return false;
            }
            $("button").attr("disabled", "");
        },
        success: function(result){
            $("button").removeAttr("disabled");
            var data = JSON.parse(result);
            if(data.result == "success"){
                swal({
                    title: "Success!",
                    text: data.message,
                    icon: "success",
                    timer: 5000,
                    button: false
                });
                $('.iziModal').iziModal('close');
            } else {
                swal({
                    title: "Error!",
                    text: data.message,
                    icon: "error",
                    button: false,
                    timer: 3000
                });
            }
        }
    });
  });

  $("#submit-form7-form").submit(function(e){
    e.preventDefault();
    var data = new FormData(this);
    $.ajax({
        url: site_url + "/ajax/form7",
        type: "POST",
        data: data,
        contentType: false,
        processData: false,
        beforeSend: function(){
            if(data.get("start").length < 1){
                swal({
                    title: "Error!",
                    text: "Start date cannot be empty!",
                    icon: "warning",
                    button: false,
                    timer: 3000
                });
                return false;
            }
            if(data.get("end").length < 1){
                swal({
                    title: "Error!",
                    text: "End date cannot be empty!",
                    icon: "warning",
                    button: false,
                    timer: 3000
                });
                return false;
            }
            $("button").attr("disabled", "");
        },
        success: function(result){
            $("button").removeAttr("disabled");
            var data = JSON.parse(result);
            if(data.result == "success"){
                swal({
                    title: "Success!",
                    text: data.message,
                    icon: "success",
                    timer: 5000,
                    button: false
                });
                $('.iziModal').iziModal('close');
            } else {
                swal({
                    title: "Error!",
                    text: data.message,
                    icon: "error",
                    button: false,
                    timer: 3000
                });
            }
        }
    });
  });

  $("#system-config-form").submit(function(e){
    e.preventDefault();
    var data = new FormData(this);
    $.ajax({
        url: site_url + "/ajax/settings",
        type: "POST",
        data: data,
        contentType: false,
        processData: false,
        beforeSend: function(){
            $("button").attr("disabled", "");
        },
        success: function(result){
            $("button").removeAttr("disabled");
            var data = JSON.parse(result);
            if(data.result == "success"){
                swal({
                    title: "Success!",
                    text: data.message,
                    icon: "success",
                    timer: 3000,
                    button: false
                });
            } else {
                swal({
                    title: "Error!",
                    text: data.message,
                    icon: "error",
                    button: false,
                    timer: 3000
                });
            }
        }
    });
  });
});