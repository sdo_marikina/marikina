$(document).ready(function(){
   $("#login").submit(function(e){
      e.preventDefault();
      var data = new FormData(this);
      $.ajax({
          url: site_url + "/ajax/login",
          type: "POST",
          data: data,
          contentType: false,
          processData: false,
          beforeSend: function(){
              if(data.get("username").length < 1){
                  swal({
                      title: "Error!",
                      text: "Username cannot be empty!",
                      icon: "warning",
                      button: false,
                      timer: 3000
                  });
                  return false;
              }
              if(data.get("password").length < 1){
                  swal({
                      title: "Error!",
                      text: "Password cannot be empty!",
                      icon: "warning",
                      button: false,
                      timer: 3000
                  });
                  return false;
              }
              $("button").attr("disabled", "");
          },
          success: function(result){
              $("button").removeAttr("disabled");
              var data = JSON.parse(result);
              if(data.result == "success"){
                  swal({
                      title: "Welcome!",
                      text: data.message,
                      icon: "success",
                      closeOnClickOutside: false,
                      closeOnEsc: false,
                      button: false
                  });
                  setInterval(function() {
                      window.location = site_url + "/admin/dashboard";
                  }, 3000);
              } else {
                  swal({
                      title: "Error!",
                      text: data.message,
                      icon: "error",
                      button: false,
                      timer: 3000
                  });
              }
          }
      });
   });
   $("#panel-form").submit(function(e){
      e.preventDefault();
      var data = new FormData(this);
      $.ajax({
          url: site_url + "/panel/verify",
          type: "POST",
          data: data,
          contentType: false,
          processData: false,
          beforeSend: function(){
              if(data.get("eid").length < 1){
                  swal({
                    title: "Error!",
                    text: "Employee number cannot be empty!",
                    icon: "warning",
                    button: false,
                    timer: 3000
                  });
                  return false;
              }
              $("button").attr("disabled", "");
          },
          success: function(result){
              $("button").removeAttr("disabled");
              var data = JSON.parse(result);
              if(data.result == "success"){
                $.getJSON(site_url + "/panel/attendance/?eid=" + data.eid, function (data) {
                  if($.isEmptyObject(data)){
                    swal({
                      title: "Error!",
                      text: "This employee has no attendance logs for this month!",
                      icon: "warning",
                      button: false,
                      timer: 5000
                    });
                  } else {
                    $('.table').createTable(data);
                  }
                });
              } else {
                swal({
                  title: "Error!",
                  text: "Employee not found!",
                  icon: "error",
                  button: false,
                  timer: 3000
                });
              }
          }
      });
   });
});