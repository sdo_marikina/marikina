$(function() {
    var oTable = $('#missed-access').DataTable({
        ajax: site_url + "/ajax/missed",
        dom: 'lfrtip',
        lengthMenu: [10, 25, 50, 75, 100],
        pageLength: 50,
        aaSorting: [[0, "asc"]],
        sPaginationType: "full_numbers",
        liveAjax: {
            interval: 5000
        },
        oLanguage: {
            sSearch: "<i class='fa fa-search'></i> Search",
            sSearchPlaceholder: "Name, Employee #, Type, Missed Access"
        },
        columns: [
            {
                title: 'Employee Number',
                width: '13%',
                data:  'eid'
            },
            {
                title: 'Name',
                data:  'name'
            },
            {
                title: 'Employment Type',
                data:  'type'
            },
            {
                title: 'Missed Access',
                data:  'missed_access'
            },
            {
                title: 'Date',
                className: "text-center",
                data:  'date'
            },
            {
                title: 'Options',
                className: "text-center",
                data:  'options',
                searchable: false,
                sortable: false
            }
        ]
    });
});