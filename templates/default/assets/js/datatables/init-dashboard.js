$(function() {
    var oTable = $('#access-logs').DataTable({
        ajax: site_url + "/ajax/attendance",
        dom: 'lfrtip',
        lengthMenu: [10, 25, 50, 75, 100],
        pageLength: 50,
        aaSorting: [[0, "asc"]],
        sPaginationType: "full_numbers",
        liveAjax: {
            interval: 1000
        },
        oLanguage: {
            sSearch: "<i class='fa fa-search'></i> Search",
            sSearchPlaceholder: "Name, Employee #, Card ID, Access Type"
        },
        columns: [
            {
                title: 'Count',
                data:  'count',
                visible: false,
                searchable: false
            },
            {
                title: 'Employee Number',
                width: '13%',
                data:  'eid'
            },
            {
                title: 'Name',
                data:  'name'
            },
            {
                title: 'Access Type',
                data:  'access'
            },
            {
                title: 'Date',
                className: "text-center",
                data:  'date'
            },
        ]
    });
});