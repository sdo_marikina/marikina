$(function() {
    $('#employees').DataTable({
        ajax: site_url + "/ajax/employees",
        dom: 'lfrtip',
        lengthMenu: [10, 25, 50, 75, 100],
        pageLength: 10,
        aaSorting: [[0, "desc"]],
        sPaginationType: "full_numbers",
        oLanguage: {
            sSearch: "<i class='fa fa-search'></i> Search",
            sSearchPlaceholder: "Name, Employee #, Card ID"
        },
        columns: [
            {
                title: 'ID',
                data:  'id',
                searchable: false,
                visible: false
            },
            {
                title: 'Employee Number',
                width: '13%',
                data:  'eid'
            },
            {
                title: 'Name',
                data:  'name'
            },
            {
                title: 'Basic Pay',
                data:  'basic'
            },
            {
                title: 'Position',
                data:  'position'
            },
            {
                title: 'Department',
                data:  'department'
            },
            {
                title: 'Employment Type',
                data:  'type'
            },
            {
                title: 'Schedule',
                data:  'schedule',
                className: "text-center",
                searchable: false,
                orderable: false
            },
            {
                title: 'Administrative Options',
                className: "text-center",
                data:  'options',
                searchable: false,
                orderable: false
            }
        ]
    });
});