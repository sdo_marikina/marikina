$(function() {
    minDateFilter = "";
    maxDateFilter = "";

    var oTable = $('#history').DataTable({
        ajax: site_url + "/ajax/history",
        dom: 'lfrtip',
        lengthMenu: [10, 25, 50, 75, 100],
        pageLength: 25,
        deferRender: true,
        aaSorting: [[4, "desc"]],
        sPaginationType: "full_numbers",
        oLanguage: {
            sSearch: "<i class='fa fa-search'></i> Search",
            sSearchPlaceholder: "Name, Employee #, Card ID, Access Type"
        },
        columns: [
            {
                title: 'Image',
                data:  'image',
                className: "text-center",
                searchable: false
            },
            {
                title: 'Employee Number',
                width: '13%',
                data:  'eid'
            },
            {
                title: 'Name',
                data:  'name'
            },
            {
                title: 'Access Type',
                data:  'access'
            },
            {
                title: 'Date',
                className: "text-center",
                data:  'date'
            },
            {
                title: 'Date Filter',
                data:  'fdate',
                visible: false
            }
        ]
    });

    $("#date-min").datepicker({
        "onSelect": function(date) {
            $(this).val(date);
            minDateFilter = new Date(date).getTime();
            oTable.draw();
        }
    }).keyup(function() {
        $(this).val(date);
        minDateFilter = new Date(this.value).getTime();
        oTable.draw();
    });

    $("#date-max").datepicker({
        "onSelect": function(date) {
            $(this).val(date);
            maxDateFilter = new Date(date).getTime();
            oTable.draw();
        }
    }).keyup(function() {
        $(this).val(date);
        maxDateFilter = new Date(this.value).getTime();
        oTable.draw();
    });
});

$.fn.dataTableExt.afnFiltering.push(
    function(oSettings, aData, iDataIndex) {
        if (typeof aData._date == 'undefined') {
            aData._date = new Date(aData[5]).getTime();
        }

        if (minDateFilter && !isNaN(minDateFilter)) {
            if (aData._date < minDateFilter) {
                return false;
            }
        }

        if (maxDateFilter && !isNaN(maxDateFilter)) {
            if (aData._date > maxDateFilter) {
                return false;
            }
        }

        return true;
    }
);