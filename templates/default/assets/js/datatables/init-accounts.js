$(function() {
    $('#accounts').DataTable({
        ajax: site_url + "/ajax/accounts",
        dom: 'lfrtip',
        lengthMenu: [10, 25, 50, 75, 100],
        pageLength: 10,
        aaSorting: [[0, "desc"]],
        sPaginationType: "full_numbers",
        liveAjax: {
            interval: 5000
        },
        oLanguage: {
            sSearch: "<i class='fa fa-search'></i> Search",
            sSearchPlaceholder: "Username"
        },
        columns: [
            {
                title: 'ID',
                data:  'id',
                className: "text-center",
                width: '7%'
            },
            {
                title: 'Username',
                data:  'username',
                className: "text-center"
            },
            {
                title: 'Options',
                data:  'options',
                className: "text-center",
                searchable: false,
                orderable: false
            }
        ]
    });
});