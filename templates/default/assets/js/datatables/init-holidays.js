$(function() {
    $('#holidays').DataTable({
        ajax: site_url + "/ajax/holidays",
        dom: 'lfrtip',
        lengthMenu: [10, 25, 50, 75, 100],
        pageLength: 10,
        aaSorting: [[0, "desc"]],
        sPaginationType: "full_numbers",
        liveAjax: {
            interval: 5000
        },
        oLanguage: {
            sSearch: "<i class='fa fa-search'></i> Search",
            sSearchPlaceholder: "Name, Date"
        },
        columns: [
            {
                title: 'ID',
                data:  'id',
                className: "text-center",
                width: '7%', 
                searchable: false
            },
            {
                title: 'Name',
                data:  'name',
                className: "text-center"
            },
            {
                title: 'Date',
                data:  'date',
                className: "text-center"
            },
            {
                title: 'Options',
                data:  'options',
                className: "text-center",
                searchable: false,
                orderable: false
            }
        ]
    });
});